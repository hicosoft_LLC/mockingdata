const {app, Menu, MenuItem, dialog, BrowserWindow}	= require('electron');
const
fs	= require('fs'),
$	= require('jquery'),
dbControl = require('./appCore/dbControl'),
append    = require('./appCore/append'),
userDB    = app.getPath('userData') + '/userDB.mockingdata',
sqlite3   = require('sqlite3').verbose()
;

var mainWindow = null;

app.on('window-all-closed', function() {
	if (process.platform != 'darwin') {
		app.quit();
	}
});

app.on('ready', function() {

	// メニューをアプリケーションに追加
	
	openWindow();
	init();
//	Menu.setApplicationMenu(menu);
});

function openWindow(){

	mainWindow = new BrowserWindow({width: 1124, height: 700}); //, 'node-integration': false
	mainWindow.loadURL('file://' + __dirname + '/index.html');

	// Open the DevTools.
	mainWindow.webContents.openDevTools();

	mainWindow.on('closed', function() {
		mainWindow = null;
	});
}

function init(){
	
	var usrDir = app.getPath('userData')
		prjDir = {
			current : usrDir + '/CurrentProject',
			temp    : usrDir + '/MockingProject'
		}
	;	
	fs.exists(prjDir.current, (exists) => {		
		if(!exists)	fs.mkdirSync(prjDir.current);
	});
	fs.exists(prjDir.temp, (exists) => {		
		if(!exists)	fs.mkdirSync(prjDir.temp);
	});
	fs.exists(userDB, (exists) => {		
		if(!exists){
			db = new sqlite3.Database(userDB);
			db.close();
		}
	});
	console.dir(process.verions)
}
// メニュー情報の作成
var template = [
	{
		label: 'MockingData',
		submenu: [
			{
				label: 'About MockingData',
				click: function(){
				}
			},
			{
				type: 'separator'
			},
			{
				label: 'Preferences',
				click: function(){
				}
			},
			{
				type: 'separator'
			},
			{
				label: 'Quit',
				accelerator: 'Command+Q',
				click: function(){
					app.quit();
				}
			}
		]
	},
	{
		label: 'File',
		submenu: [
			{
				label: 'Open',
				accelerator: 'Command+O',
				click: function(){	// 「ファイルを開く」ダイアログの呼び出し
					require('dialog').showOpenDialog(
						{
							properties: ['openDirectory']
						},
						function (baseDir){
							if(baseDir && baseDir[0]){
								openWindow(baseDir[0]);
							}
						}
					);
				}
			}
		]
	},
	{
		label: 'Column',
		submenu: [
			{
				label: 'まとめて設定',
				click: function(){
					append.modal.open('column');
				}
			},
			{
				label: '末尾に追加',
				click: function(){
				}
			},
			{
				label: '選択した列を削除',
				click: function(){
				}
			},
			{
				type: 'separator'
			},
			{
				label: '左揃え',
				click: function(){

				}
			},
			{
				label: '中央揃え',
				click: function(){
				}
			},
			{
				label: '右揃え',
				click: function(){
				}
			},
		]
	},
	{
		label: 'DataType',
		submenu: [
			{
				label: '連番',
				click: function(){
					append.modal.open('increment');
					append.form.columnOption();
				}
			},
			{
				label: 'ランダム',
				click: function(){

				}
			},
			{
				label: 'パターン',
				click: function(){

				}
			},
			{
				label: '任意',
				click: function(){

				}
			},
			{
				label: '個人名',
				click: function(){

				}
			},
			{
				label: '連番',
				click: function(){

				}
			},
			{
				label: '住所',
				click: function(){

				}
			},
			{
				label: 'メールアドレス',
				click: function(){

				}
			},
			{
				label: '電話番号',
				click: function(){

				}
			},
			{
				label: '日時',
				click: function(){

				}
			},
			{
				label: '長文',
				click: function(){

				}
			},
			{
				label: 'Database',
				click: function(){

				}
			}
		]
	},
	{
		label: 'Grid',
		submenu: [
			{
				label: 'Refresh',
				click: function(){
				}
			},
			{
				type: 'separator'
			},
			{
				label: 'Clear',
				click: function() {
				}
			}
		]
	}
];

var menu = Menu.buildFromTemplate(template);
