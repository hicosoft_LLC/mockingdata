/*
console.log('start');

function puts(str) {
  return new Promise(function(resolve) {
    setTimeout(function() {
      resolve(str);
    }, 1000);
  });
}


var promises = [
  puts(1),
  puts(2),
  puts(3)
];

// 並列処理に対してPromise.allを使う
Promise.all(promises).then(function(results) {
  console.log(results);
}).catch(function onRejected(error){
  console.error(error);
});

console.log('end');

const treble = function(number) {
  return new Promise(function(resolve) {
    resolve(number * 3);
  });
};

// 同期処理: ログに表示する
const dump = function(number) {
  console.log(number);
  return number;
};

treble(10) // 10 * 3 -> 30
  .then(dump)
  .then(treble) // 30 * 3 -> 90
  .then(dump)
  .then(treble) // 90 * 3 -> 270
  .then(dump);
*/
/*
	var filename = __filename;
	var crypto = require('crypto');
console.log(filename);
	var shasum = crypto.createHash('sha512');
	
	
	var s = fs.ReadStream(filename);
	s.on('data', function(d) {
	  shasum.update(d);
	});

	s.on('end', function() {
	  var d = shasum.digest('hex');
	  console.log(d + '  ' + filename);
	});
*/

var header, sideBar, main,
	activeCss = 'sidebar-active-bg'
;
// function resolveAfter2Seconds(){
// 	var	dbPath	= path.resolve(__dirname, 'appCore/dbControl/mockingMaster.sqlite'),
// 		db		= new sqlite3.Database(dbPath),
// 		sql		= 'SELECT * FROM domainTop LIMIT 20'
// 	;

// 	return new Promise(function(resolve, reject) {
// 		db.all(sql, function (err, rows){
// 			console.log(rows);
// 			resolve(rows);
// 		});
// 	});
// }
// var rs = async(function(){
//   var x = await2(resolveAfter2Seconds());
  
//   return x;
// });

// console.log(rs);

$(function(){
		
	header = $('.toolbar-actions');
	sideBar= $('.sidebar');
	main   = $('.main-content');

	$("#confirmGrid thead").change(function(){
		$("#confirmGrid").resizableColumns();
	});
	
	defaultView();
	
	$('#btnOpen', header).click(function(){
		appSrc.appFile.deploy();
	});
	//File
	$('#btnSave', header).click(function(){		
		appSrc.appFile.save();
	});
	$('#btnAddPattern', header).click(function(){
		patternModal('save');
	});
	$('#btnExport', header).click(function(){
		patternModal('export');
	});
	$('#btnToDatabase', header).click(function(){
		
	});
	$('#btnRecycle', header).click(function(){

		current	= $.extend(true, {}, appSrc.config.current);
		append.grid.recycle();
		$('.group-list div', sideBar).removeClass(activeCss);
		
	});
	
	//sideBar
	$('#btnSlideBar', header).click(function(){
		$("#sideBar").slideToggle(0);
		var viw = $("#sideBar").css('display')
			icnClose= "<i class='fa fa-arrow-left'></i>]"
			icnOpen = "<i class='fa fa-arrow-right'></i>]"
		;
		$(this).children().remove();
		$(this).text('');
		if(viw == 'none')	$(this).append(icnOpen);
		else				$(this).append(icnClose);
	});
	
	//Columns
	$('#btnColumns', header).click(function(){
		append.modal.open('column');
	});
	$('#btnAddColumn', header).click(function(){
		var newColName = append.grid.column.add();
		append.grid.emptyCell([newColName]);
	});
	$('#btnRemoveColumn', header).click(function(){
		append.grid.column.remove();
	});
	
	//Data patterns
	patternAction();

	//MaxRows
	$('#dataCount', header).change(function(){
		var usingParam = ['dataCount'];
		if(!validate.isValid(usingParam)){
			console.log(validate.error);
			return;
		}
		current.dataCount = validate.parameter.valid['dataCount'];
		current.maxRows   = (current.dataCount < appSrc.config.maxRows)? current.dataCount : appSrc.config.maxRows;
		append.grid.refresh();
	});
	//Grid
	$('#btnRefresh', header).click(function(){
		
   		var gen = null;
   		
   		gen = new generator.export;
   		gen.preview = true;
   		gen.exec(current)
   		.then(function(dataList){
   			append.grid.clear();
	   		append.grid.head(current.column.nameList);
	   		append.grid.row();
	   		append.grid.emptyCell(current.column.nameList);
   			append.grid.dataAll(dataList);
   			current.column.index	= -1;
   			
   			if(current.setPatternName){
				appSrc.pattern.save(project[current.groupName][current.setPatternName]);
			}
   		});
	});
	$('#btnTrash', header).click(function(){
		
		var grid    = $('#confirmGrid'),
			confBtn = appSrc.config.enum.button,
			status  = appSrc.config.enum.status
		;
		if(0 < current.column.nameList.length){
			var msgObj = {
				subject : 'パターンデータを作成中です。',
				message : '作成したパターンデータを保存しますか。'
			}
			var btnId = appSrc.alert.confirm(msgObj);
			if(btnId == confBtn.Yes){
				patternModal('save');
				return;
			}
		}
		/*
			現在のパターンデータを
		*/
		current.column.index	= -1;
		current.column.nameList	= [];
		current.column.nameList	= [];
		current.patternList		= [];
	//	保存するか確認
		$('thead tr', grid).children().remove();
		$('tbody', grid).children().remove();
	});
	
	
	//Text align
	$('#btnAlignLeft', header).click(function(){		
		currentCells().removeClass('cell-center cell-right').addClass('cell-left');
		cellAlign('left');
	});
	$('#btnAlignCenter', header).click(function(){
		currentCells().removeClass('cell-left cell-right').addClass('cell-center');		
		cellAlign('center');
	});
	$('#btnAlignRight', header).click(function(){
		currentCells().removeClass('cell-left cell-center').addClass('cell-right');	
		cellAlign('right');	
	});
	
	// Development
	$('#btnDevTool', header).click(function(){
		remote.getCurrentWindow().openDevTools();
	})
	$('#btnCogs', header).click(function(){
		console.log({'project':project, 'current':current, 'delegate':delegate});
	});
	
	//
	$('#addGroup', main).click(function(){
		patternModal('save');
	});	
	

});

function defaultView(){
	
	var project = appSrc.project.get(),
		tags	= dataBind.sideMenu(project),
		elmGrp	= $('.group-list', sideBar)
	;
	elmGrp.children().remove();
	elmGrp.append(tags.join('\n'));
	
	$('.group-list > div', sideBar).click(function() {
		
		append.modal.close();
		$(this).find('+.setpattern-list').stop(true, true).slideToggle(200);
		$('.group-list div', sideBar).removeClass(activeCss);
	});
	$('.group-list > div', sideBar).on('contextmenu', function(){
		
		append.modal.close();
		$('.group-list div', sideBar).removeClass(activeCss);
        $(this).addClass(activeCss);
		append.contextMenu.project.group($(this).text());
	});
	$('.setpattern-list > div', sideBar).click(function(e) {

		append.modal.close();
        e.stopPropagation();
        $('.group-list div', sideBar).removeClass(activeCss);
        $(this).addClass(activeCss);
   
   		current = appSrc.pattern.get($(this).attr('fileName'));
   		
   		$('#dataCount').val(current.dataCount);   		
   		
   		var gen = null;
   		$(this).append('<i class="fa fa-cog fa-spin fa-3x fa-fw icon-white"></i>');
   		gen = new generator.export;
   		gen.preview = true;
   		gen.exec(current)
   		.then(function(dataList){
   			
   			append.grid.clear();
	   		append.grid.head(current.column.nameList);
	   		append.grid.row();
	   		append.grid.emptyCell(current.column.nameList);
   			append.grid.dataAll(dataList);
   			
   			var grid  = $('#confirmGrid'),
				gHead = $('thead', grid),
				gBody = $('tbody', grid),
				numCol = current.column.index + 1,
				cssGrid = cssClass.grid
			;
			$("tr th", gHead).removeClass(cssGrid.head.selected);
			$('tr td', gBody).removeClass(cssGrid.body.selected);
			$('tr th:nth-child(' + numCol + ')', gHead).addClass(cssGrid.head.selected);
			$('tr td:nth-child(' + numCol + ')', gBody).addClass(cssGrid.body.selected);
   		})
   		.then(function(){
   			$('.icon-white').remove();
   		})
    });
    $('.setpattern-list > div', sideBar).on('contextmenu', function(){
    	
    	append.modal.close();
    	$('.group-list div', sideBar).removeClass(activeCss);
        $(this).addClass(activeCss);
        var grpKey	= $(this).parent().prev().text(),
        	setKey	= $(this).text()
        ;
		current = appSrc.pattern.get(project[grpKey][setKey]);
		current.groupName		= grpKey;
		current.setPatternName	= setKey;
		
    	append.contextMenu.project.setPattern($(this));
    })
}
function patternAction(){
	var header = $('.toolbar-actions'),
		ptnName = [	'Increment','RandomNumber','RandomString','Regex','Encryption',
					'Anything','Person','Address','Email',
					'Phone','Datetime','LongText','Database'];
	
	$.each(ptnName, function(idx, elm){
		$('#btn' + elm, header).click(function(){
			patternModal(elm.toLowerCase());
		});
	});
}
function patternModal(ptnName){
	append.modal.open(ptnName);
}
function currentCells(){
	return $('#confirmGrid tr td:nth-child(' + (current.column.index + 1) + ')');
}
function cellAlign(align){
	var key = Object.keys(current.patternList)[current.column.index];	
	current.patternList[current.column.index].textAlign = align;
}