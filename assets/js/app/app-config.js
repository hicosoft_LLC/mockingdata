const 
{remote} = require('electron'),
{app, Menu, MenuItem, dialog, BrowserWindow, shell}	= remote
;

const
fs      = require('fs'),
fse     = require('fs-extra'),
util    = require('util'),
path    = require('path'),
os      = require('os'),
JSON5   = require('json5'),
q       = require('q'),
crypto  = require("crypto"),
fstream = require('fstream'),
zlib	= require("zlib"),
tar		= require("tar"),
sleep	= require("sleep"),
sqlite3 = require('sqlite3').verbose(),
clipboard = remote.clipboard
;

const
append    = require('./appCore/append'),
appEvent  = require('./appCore/appEvent'),
generator = require('./appCore/generator'),
dataBind  = require('./appCore/dataBind'),
dbControl = require('./appCore/dbControl'),
entry     = require('./appCore/entry'),
run       = require('./appCore/event'),
appSrc    = require('./appCore/sources'),
validate  = require('./appCore/validate'),
to        = require('./appCore/convert'),
crawler   = require('./appCore/crawler')
;



var
project	   = appSrc.project.get(),
delegate   = generator.delegate,
current    = $.extend(true, {}, appSrc.config.current);
saveStatus = appSrc.config.none,
iniMessage = {
	subject : '',
	message : ''
},
cssClass   = appSrc.config.css
;

console.log(generator.random.number);
