/**
 *
 **/
module.exports = {
	preview     : false,
	address		: require('./src/address'),
	anything	: require('./src/anything'),
	blank	    : require('./src/blank'),
	column		: require('./src/column'),	
	database	: require('./src/database'),
	datetime	: require('./src/datetime'),
	delegate    : require('./src/delegate'),
	email		: require('./src/email'),
	encryption	: require('./src/encryption'),
	export		: require('./src/export'),
	increment	: require('./src/increment'),
	longtext	: require('./src/longtext'),
	person		: require('./src/person'),
	phone		: require('./src/phone'),
	random		: require('./src/random'),
	regex		: require('./src/regex')
}	