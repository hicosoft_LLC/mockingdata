/**
 ja {max: 572725, one: 10658,  two: 69, three: 561998, other: 0}
 en {max: 561411, one: 554619, two: 14, three: 6778,   other: 0}
 **/
var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
}

generator.prototype.exec = function(){
	
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var self    = this,
		param	= self.parameter,
		list    = [],			
		locale  = appSrc.locale,
		txtBody = appSrc.longtext(param.longTextFile),
		fullLen	= txtBody.length,
		unit    = (locale == 'ja')? 3 : 1,
		range   = {
			min : param.minTextRange,
			max : param.maxTextRange
		},
		randNo	= 0,
		maxLen	= 0,
		startNo	= 0,
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	
	return new Promise(function(resolve, reject) {
		
		for(i = 0; i < genNumber; i++){
						
			randNo  = Math.floor(Math.random() * (range.max)) + range.min;
			maxLen	= Math.floor(Math.random() * (range.max / unit)) + range.min;			
			startNo = Math.floor(Math.random() * fullLen) - Math.floor(range.max / unit) - 1;

			list.push(txtBody.substr(startNo, maxLen));
		}
		self.dataList = list;
		resolve(list);
	});
}	

module.exports = generator;

var byteCounter = {
	UTF8 : function(str){
		return(encodeURIComponent(str).replace(/%../g,"x").length); 
	},
	ShiftJIS : function(){
		var r = 0;
		for (var i = 0; i < s.length; i++) {
			var c = s.charCodeAt(i);
			// Shift_JIS: 0x0 ～ 0x80, 0xa0  , 0xa1   ～ 0xdf  , 0xfd   ～ 0xff
			// Unicode  : 0x0 ～ 0x80, 0xf8f0, 0xff61 ～ 0xff9f, 0xf8f1 ～ 0xf8f3
			if ( (c >= 0x0 && c < 0x81) || (c == 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4)) {
				r += 1;
			} else {
				r += 2;
			}
		}
		return r;
	}
}



