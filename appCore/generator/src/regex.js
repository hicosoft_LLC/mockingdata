/**
	・以下の記号及びメタ文字に対応する
	・[+*?]等の任意の文字には対応しない。必ず文字範囲を指定すること
	・[^]否定集合は使用不可 -> 対象がほぼ無限大になってしまうため
	・文字数範囲{1,}使用不可 -> 最大文字数が指定されていない。
	
	(){}[]{}.+|
	メタ文字 : 
		\d : 数字 [0-9]
		\D : 数字以外 [^0-9]
		\s : スペース、タブ、改ページ、改行を含む 1 個のホワイトスペース文字
		         [ \f\n\r\t\v​\u00a0\u1680​\u180e\u2000​-\u200a​\u2028\u2029\u202f\u205f​\u3000\ufeff]
		\S : ホワイトスペース以外の 1 文字
		         [^ \f\n\r\t\v​\u00a0\u1680​\u180e\u2000-\u200a​\u2028\u2029​\u202f\u205f​\u3000\ufeff]
		\t : tab
		\w : 英数字 + アンスコ [A-Za-z0-9_]
		\W : [^A-Za-z0-9_]
	
	例)
		UUID :	550e8400-e29b-41d4-a716-446655440000 (Ver.4)
				[a-z0-9]{8}-[a-z0-9]{4}-4[a-z0-9]{3}-r[a-z0-9]{3}-[a-z0-9]{10}
 **/
var errMessage = []

;
var generator = function(){
	
	this.parameter   = {};
	this.patternList = [];
	this.dataList    = [];
	this.preview     = true;
	this.error       = errMessage;
}

generator.prototype.exec = function(){
		
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var self   = this,
		param  = self.parameter,
		regPtn = /\([!-~]{1,}\)|\[[^\[]{1,}\]\{[0-9,]{1,}\}|([^\(\)\{\}\[\]]{1,})/g,
		ptnSet = [],
		list   = [],		
		str    = param.regexPattern,
		ary    = str.match(regPtn),
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	
	return new Promise(function(resolve, reject){
		
		for(var i = 0; i < ary.length; i++){
			
			if(ary[i].match(/^\(.+\)$/)){
				var elm = self.patternElement.specific(ary[i]);
				ptnSet.push(elm);
			}
			if(ary[i].match(/^\[.+\}$/)){
				var elm = self.patternElement.subset(ary[i]);
				ptnSet.push(elm);
			}
			if(ary[i].match(/^[^\[\(]{1,}/)){
				ptnSet.push(ary[i]);
			}
		}
		var item = [];
		for(var i = 0; i < genNumber; i++){
			
			item = [];
			for(var j = 0; j < ptnSet.length; j++){
				if(Array.isArray(ptnSet[j])){
					item.push(self.dataElement.specific(ptnSet[j]));
					continue;
				}
				if(typeof(ptnSet[j]) == 'object'){
					item.push(self.dataElement.subset(ptnSet[j]));
					continue;
				}	
				if(typeof(ptnSet[j]) == 'string'){
					item.push(ptnSet[j]);
					continue;
				}	
			}
			list.push(item.join(''));
		}
		resolve(list);
	});
}
	
generator.prototype.patternElement = {
	
	specific : function(pattern){
		
		var str = pattern.replace(/^\(/, '').replace(/\)$/, '');
		
		return str.split('|');
	},
	subset : function(pattern){
		
		var charSet   = pattern.match(/^\[.+\]/),
			range     = pattern.match(/\{[0-9,]{1,}\}$/),
			strChars  = charSet[0].replace(/^\[/, '').replace(/\]$/, ''),
			strRange  = range[0].replace(/^\{/, '').replace(/\}$/, ''),
			rangeChar = charRange(strChars);
			rangeLen  = lengthRange(strRange),
			ptn = {
				charSet : rangeChar,
				min     : rangeLen[0],
				max     : rangeLen[1]
			}
		;
		return ptn;
	}
}

generator.prototype.dataElement = {
	
	specific : function(ary){
		
		return ary[Math.floor(Math.random() * ary.length)];
	},
	subset : function(obj){
		
		var str = ''
			addLen = 0;
		;
		if(obj.min == null){				
			for(j = 0; j < obj.max; j++){
				str += obj.charSet[Math.floor(Math.random() * obj.charSet.length)];
			}
			return str;
		}
		else{
			for(var i  = 0; i < obj.min; i++){
				str += obj.charSet[Math.floor(Math.random() * obj.charSet.length)];
			}
			addLen = Math.floor(Math.random() * (obj.max - obj.min));
			for(var i  = 0; i < addLen; i++){
				str += obj.charSet[Math.floor(Math.random() * obj.charSet.length)];
			}                  
			return str;
		}
	}
}

module.exports = generator;

function lengthRange(str){
	
	var elms = [];
	if(str.indexOf(',') < 0){
		elms[0]	= null,
		elms[1] = Number(str);
	}				
	else{
		var items = str.split(',');
		elms[0]	= Number(items[0]),
		elms[1] = Number(items[1]);
	}
	return elms;
}
function charRange(str){
	
	var ptnRange = str.match(/.-./g),
		ptnSpec  = str,
		list	 = []
	;
	
	for(var i = 0; i < ptnRange.length; i++){
		ptnSpec = ptnSpec.replace(ptnRange[i], '');
		
		var fromTo = ptnRange[i].split('-');
		Array.prototype.push.apply(list, range(fromTo[0], fromTo[1]));
	}
	Array.prototype.push.apply(list, ptnSpec.split(''));

	return list;
}
function range(from, to){
	
	var min	= from.charCodeAt(),
		max	= to.charCodeAt(),
		ary = []
	;
	for(i = min; i <= max; i++){
		ary.push(String.fromCharCode(i));
	}
	return ary;
}