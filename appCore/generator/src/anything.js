/**
 *
 
実データ件数以上の行を生成するSQL
---------------------------------
WITH RECURSIVE
counter(x, y) AS (
	VALUES(1, 0) 
	UNION ALL 
	SELECT x+1, random() FROM counter WHERE x < 50
)

この例ではJOINする[実データ件数 * 50]が生成されるため、
LIMITをつけるか[RECURSIVE]の件数を実件数で割ったものにする
---------------------------------
SELECT 
	sample.name, 
	cnt.x 
FROM cnt, sample
ORDER BY random()
LIMIT 1, 50

 **/
const xml2json = require('xml2json');

var generator = function(){
	
	this.parameter = {};
	this.dataList  = [];
	this.filePath  = '';
	this.fileBody  = '';
	this.preview   = true;
};

generator.prototype.parseFile = function(filePath){
		
	var	self    = this,
		src		= fs.readFileSync(filePath, 'utf8'),
		extn    = path.extname(filePath).toLowerCase()
	;
	self.filePath	= filePath;
	self.fileBody	= src;
	
	switch(extn){
	case '.csv':	return self.parseCsv();
	case '.txt':	return self.parseText();
	case '.json':	return self.parseJson();
	case '.json5':	return self.parseJson5();
	case '.xml':	return self.parseXml();
	default:		return self.parseDsv();
	}
}
generator.prototype.exec = function(){
		
	if(arguments.length === 2){
		return this.execObj(arguments[0], arguments[1]);			
	}

	var self    = this,
		param	= self.parameter,
		src		= arguments[0],
		anyObj  = {},
		srcLen	= src.length,
		list    = [],
		item	= '',
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	
	// 配列の判定 : Array.isArray(var)
	return new Promise(function(resolve, reject){
		for(i = 0; i < genNumber; i++){
					
			item = src[Math.floor(Math.random() * srcLen)];
			list.push(item);
		}
		self.dataList = list;
		resolve(list);
	});
}
generator.prototype.execObj = function(src, parentList){
		
	var	list = [],
		opts = [],
		item = ''
	;
	return new Promise(function(resolve, reject){
		$.each(parentList, function(idx, elm){
			
			opts = src[elm];
			item = opts[Math.floor(Math.random() * (opts.length))];
			list.push(item);
		});
		resolve(list);
	});
}
generator.prototype.parseCsv = function(){
		
	var self     = this,
		lineList = '',
		line     = '',
		list     = []
	;
	try{
		lineList = self.fileBody.split('\n');
	}
	catch(e){
		self.alert(e);
		return list;
	}
	for(var i = 0; i < lineList.length; i++){
		
		line = $.trim(lineList[i]).replace(/,$/, '').replace(/\t/g, ',').replace(/\"/g, '');
		if(line != ''){
			Array.prototype.push.apply(list, line.split(','));
		}
	}
	return list;
}
generator.prototype.parseText = function(){
		
	var self    = this,
		lineList = '',
		line     = '',
		list     = []
	;
	try{
		lineList = self.fileBody.split('\n');
	}
	catch(e){
		self.alert(e);
		return list;
	}
	for(var i = 0; i < lineList.length; i++){
		
		line = $.trim(lineList[i]);
		if(line != ''){
			Array.prototype.push.apply(list, line.split('\t'));
		}
	}
	return list;
}
generator.prototype.parseJson = function(){
		
	try{
		return JSON.parse(this.fileBody);
	}
	catch(e){
		this.alert(e);
		return {};
	}
}
generator.prototype.parseJson5 = function(){
		
	try{
		return JSON5.parse(this.fileBody);
	}
	catch(e){
		this.alert(e);
		return {};
	}
}
generator.prototype.parseXml = function(){

	try{
		return xml2json.toJson(this.fileBody);
	}
	catch(e){
		this.alert(e);
		return {};
	}
}
generator.prototype.parseDsv = function(){
		
	var self    = this,
		param	= self.parameter,
		lineList = '',
		line     = '',
		list     = [],
		msg      = appSrc.message.fileRead.warning
	;
	try{
		lineList = self.fileBody.split('\n');
	}
	catch(e){						
		self.alert(e);
		return list;
	}
	for(var i = 0; i < lineList.length; i++){
		
		line = $.trim(lineList[i]);
		if(line != ''){
			Array.prototype.push.apply(list, line.split(param.delimiter));
		}
	}
	return list;
}
generator.prototype.alert = function(e){
		
	var msg = appSrc.message.fileRead.warning;
	msg.description = e.message;
	appSrc.alert.info(msg);			
}

module.exports = generator;