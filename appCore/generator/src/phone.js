/**
 * 普及台数(2015) : 固定電話 = 2507, IP電話 = 3075, 携帯電話 = 16276(万件)
 **/
const
regex = require('./regex')

;
var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
}

generator.prototype.exec = function(){
		
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var self      = this,
		param     = self.parameter,
		genNumber = (self.preview)? current.maxRows : current.dataCount,
		landNum   = genNumber * (param.rateLandline / 100),
		cellNum   = genNumber - landNum,
		shuffle   = function() {return Math.random()-.5}
	;
	
	return new Promise(function(resolve, reject){
		
		self.landline([], landNum)
		.then(function(landList){
			return self.cellphone(landList, cellNum);
		})
		.then(function(cellList){
			resolve(cellList.sort(shuffle));
		})
	});
}

generator.prototype.landline = function(list, genNum){
	
	var self    = this,
		param	= self.parameter
	;
/*
SELECT
	*
FROM	tempCityCode	AS city,
		tempPhone		AS phone
WHERE	city.cityName LIKE phone.localName || '%'
  AND	phone.localName	= '西尾'
LIMIT 10
;
SELECT
	*
FROM	tempCityCode	AS city,
		tempPhone		AS phone
WHERE	city.cityName LIKE phone.localName || '%'
--  AND	phone.localName	= '西尾'
	ORDER BY RANDOM()
	LIMIT 10
;
SELECT
	substr(cityCode, 1, 5)
FROM tempCityCode
WHERE cityName='西尾市'
LIMIT 10
;

*/
	return new Promise(function(resolve, reject) {
		
		if(genNum == 0){
			resolve(list);
			return;
		}
		resolve([1,2]);
	});
}

generator.prototype.cellphone = function(list, genNum){
		
	var self    = this,
		param	= self.parameter
	;
	return new Promise(function(resolve, reject) {
		
		if(genNum == 0){
			resolve(list);
			return;
		}
		
		var gen		= null,
			ptnStr	= '',
			ptnObj	= {}
		;
		switch(param.phoneItem){
		case 'all':		
			switch(param.phoneDelimiter){
			case 1:		ptnStr = '(080|090)-[0-9]{4}-[0-9]{4}';	break;
			case 2:		ptnStr = '(080|090) [0-9]{4} [0-9]{4}';	break;
			default:	ptnStr = '(080|090)[0-9]{4}[0-9]{4}';	break;
			}
			break;
		case 'area':	ptnStr = '(080|090)';	break;
		default:		ptnStr = '[0-9]{4}';	break;
		}		
		
		ptnObj.regexPattern	= ptnStr;
		gen = new regex;
		gen.exec(ptnObj)
		.then(function(dataList){
			Array.prototype.push.apply(list, dataList);
			resolve(list);
		});	
	});
}

function shuffle(list){
	
}
/* ランダムに並び替え
var shuffle = function() {return Math.random()-.5};
var data = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

data.sort(shuffle) //-> ["G","Z","P","E","W","C","L","O","R","J","X","S","V","A","D","I","U","T","F","Q","H","B","Y","M","K","N"]
*/
module.exports = generator;
