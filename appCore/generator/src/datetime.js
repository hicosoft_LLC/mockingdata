/**
	infoType	: date, time
	dateFrom	: 日付 From
	dateTo		: 日付 To
	timeFrom	: 時刻 From 
	timeTo		: 時刻 To
	dateLocation: ja = 日本語, en = 英語
	dateFormat	: フォーマット文字列
	dateSort	: asc = ASC, desc = DESC, random = Random
 **/
const appSrc = require('../../sources');

var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
}

generator.prototype.exec = function(){
	
	if(arguments.length == 1)	this.parameter = arguments[0];	
	
	var self    = this,
		param	= self.parameter,
		list    = [],
		genNumber = (self.preview)? current.maxRows : current.dataCount,
		dateFormat = (param.dateFormat == '')?	'yyyy-mm-dd': param.dateFormat,
		timeFormat = (param.dateFormat == '')?	'hh:nn'		: param.dateFormat,
		strFormat  = (-1 < ['datetime', 'date'].indexOf(param.infoType))?	dateFormat	: timeFormat		
	;
	param.genNumber	= genNumber;
	param.strFormat	= strFormat;
	
	if(param.infoType == 'datetime')	return self.dateList();
	if(param.infoType == 'date')		return self.dateList();
	if(param.infoType == 'time')		return self.timeList();
}

generator.prototype.dateList = function(){
		
	var self    = this,	
		param	= self.parameter,	
	 	list    = [],
		iFrom	= new Date(param.dateFrom).getTime(),
		iTo		= new Date(param.dateTo).getTime(),
		deff	= iTo - iFrom,
		sortName= appSrc.master.seletOptions.sort[param.dateSort].toLowerCase()
	;
	
	return new Promise(function(resolve, reject) {
		switch(sortName){
			case 'asc':
				
				var step = deff / param.genNumber,
					i    = iFrom
				;			
				while(i < iTo){				
					list.push(to.datetime(param.strFormat, new Date(i), param.dateLocation));
					i += step;
				}
				break;
					
			case 'desc':
				
				var step = deff / param.genNumber,
					i    = iTo
				;			
				while(iFrom < i){				
					list.push(to.datetime(param.strFormat, new Date(i), param.dateLocation));
					i -= step;
				}
				break;

			case 'random':
				
				var iDate = 0;
				for(var i = 0; i < param.genNumber; i++){
					
					iDate	= Math.floor(Math.random() * deff) + iFrom;
					list.push(to.datetime(param.strFormat, new Date(iDate), param.dateLocation));
				}
				break;
			default: break;
		}
		self.dataList = list;
		resolve(list);
	});
}

generator.prototype.timeList = function(){
	
	var self    = this,
		param	= self.parameter,
		today   = to.datetime('yyyy/mm/dd', new Date(), param.dateLocation);
		iFrom	= new Date(today + ' ' + param.timeFrom + ':00').getTime(),
		iTo		= new Date(today + ' ' + param.timeTo + ':00').getTime(),
		sortName= appSrc.master.seletOptions.sort[param.dateSort].toLowerCase(),
		deff	= iTo - iFrom
		list    = []
	;
	return new Promise(function(resolve, reject) {
		switch(sortName){
			case 'asc':
				
				var step = deff / param.genNumber,
					i    = iFrom,
					ranNo= 0
				;			
				while(i < iTo){	
					
					ranNo	= Math.floor(Math.random() * step) + i;
					list.push(to.datetime(param.strFormat, new Date(ranNo), param.dateLocation));
					i += step;
				}				
				break;
					
			case 'desc':
				
				var step = deff / param.genNumber,
					i    = iTo,
					ranNo= 0
				;			
				while(iFrom < i){
					
					ranNo	= Math.floor(Math.random() * step) + i;		
					list.push(to.datetime(param.strFormat, new Date(ranNo), param.dateLocation));
					i -= step;
				}
				break;

			case 'random':
				
				var iDate = 0;
				for(var i = 0; i < param.genNumber; i++){
					
					iDate	= Math.floor(Math.random() * deff) + iFrom;
					list.push(to.datetime(param.strFormat, new Date(iDate), param.dateLocation));
				}
				break;
			default: break;
		}
		self.dataList = list;
		resolve(list);
	});
}

module.exports = generator;