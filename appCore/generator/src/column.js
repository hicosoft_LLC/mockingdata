/**
 * 配列の途中に要素を挿入 : ary.splice( 2, 0, "う" ) ;
 									  (index. howmeny, elm(カンマ区切りで複数可))
 **/
const
appSrc  = require('../../sources')
;

module.exports = {
	parameter : {},
	dataList  : [],
	exec : function(columnName){
		
		var iCol= current.column.nameList.indexOf(columnName);
		
		//既存の列名の場合は処理終了
		if(-1 < iCol)	return;
		
		var grid  = $('#confirmGrid'),
			gHead = $('thead tr', grid),
			gBody = $('tbody', grid),
			defValue = appSrc.config.default,
			colName = (columnName == '')? defValue.columnName : columnName;
		;		
		
		//currentデータを編集
		current.column.nameList.push(colName);	
		current.column.index = current.column.nameList.length - 1;		
		
		var gridRowSize = $('tr', gBody).size(),
			gridCellSize= $('th', gHead).size(),
			headerSize  = current.column.nameList.length
		;
		
		//gridに列を追加
		gHead.append('<th>' + colName + '</th>');
		
		if(gridRowSize < current.maxRows){
			for(var i = gridRowSize; i < current.maxRows; i++){
				gBody.append('<tr></tr>');
			}
		}
		if(gridCellSize < headerSize){
			
			$('tr', gBody).each(function(idx, elm){
				
				for(var i = gridCellSize; i < headerSize; i++){
					$(this).append('<td>&nbsp;</td>');
				}
			});
		}
		appEvent.grid.head();		
	}	
}