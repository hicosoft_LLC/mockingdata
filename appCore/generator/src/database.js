/**
 *
 **/
const sqlite3 = require('sqlite3');

var generator = function(){
	
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
};

generator.prototype.exec = function(sql){
	
	var dbPath	= '/Users/masahiko/MyApp/AppSources/MockingData/appCore/dbControl/mockingMaster.sqlite',
		db = new sqlite3.Database(dbPath),
		self = this,
		list = [],
		genNumber = 1000000
	;
	var promise = new Promise(function(resolve, reject) {
		
		function runSql(){ 
			
			return new Promise(function(resolveFunc, rejectFunc) {    
                    
				db.all( 
						sql,
						function(err, rows) {
							if(rows){
                  				Array.prototype.push.apply(delegate.domain, rows);
                  				resolveFunc();
							}
						}
				);
			})
			.then(function(){
				if(delegate.domain.length < genNumber){
					runSql();
				} 
				else{
					resolve(true);
				}
			});
		}
		runSql();
	});

	return promise;
}

module.exports = generator;

/*
var conn = null;
var transaction = new Promise(function(resolve, reject){
  // データベースのコネクションを取得する
  ConnectionPool.getConnection(parameters, function callback(err, connection){
    if(err){reject();}
    conn = connection;
    resolve();
  });

}).then(function(){
  // 何かしらQueryを流して、その後の処理を実行とか
  return new Promise(function(resolve, reject){
    conn.query(...省略)
  });

}).then(function(){
  // 問題なくここまでくればcommit。
  if(conn) {
    conn.commit();
  }

}).catch(function(err){
  console.log(err);
  // エラーがcatchされたらrollback。
  if(conn) {
    conn.rollback();
  }

}).then(function(){
  // 最後に必ずコネクションを解放する
  if(conn) {
    ConnectionPool.release(conn);
  }
});
*/



var looper = new Promise(function(resolver, rejecter) {
	function loop(i) {
		return new Promise(function(resolve, reject) {
			setTimeout(	function() {
							console.log(i);
							resolve(i+1);
						}, 
						100);
		})
		.then(function(count) {			
			if(10 <= count){
				resolver(count);
			}
			else{
				loop(count);
			}
		});
	}
//	loop(0);	
});

looper.then(function(res){
	console.log(res);
});
/*
// ループ処理の完了を受け取るPromise
-----------------------------------------------------------
new Promise(function(res, rej) {
  // ループ処理（再帰的に呼び出し）
  function loop(i) {
    // 非同期処理なのでPromiseを利用
    return new Promise(function(resolve, reject) {
      // 非同期処理部分
      setTimeout(function() {
        console.log(i);
        // resolveを呼び出し
        resolve(i+1);
      }, 100);
    })
    .then(function(count) {
      // ループを抜けるかどうかの判定
      if (count > 10) {
        // 抜ける（外側のPromiseのresolve判定を実行）
        res();
      } else {
        // 再帰的に実行
        loop(count);
      }
    });
  }
  // 初回実行
  loop(0);
}).then(function() {
  // ループ処理が終わったらここにくる
  console.log(“Finish”);
})
*/