/*
	0	"モジュラス10 (ウェイト2・1分割(Luhn formula))",
		1. 数値の各桁に、下の桁から２・１・２・１・…の順番に係数（ウェイト）を掛けます。
		2. 各桁の結果が2桁の場合には、十の位と一の位を足します。
		3. それぞれの合計を求めます。
		4. 合計を10で割り、余りを求めます（モジュラス）。
		5. この余りを 10 から引いたもの(10 - 余り)がチェックデジットです。余りが0の場合はチェックデジットも「0」になります。

	1	"モジュラス10 (ウェイト2・1一括)",
		1. 数値の各桁に、下の桁から２・１・２・１・…の順番に係数（ウェイト）を掛けます。
		2. 各桁の結果の合計を求めます。
		3. 合計を10で割り、余りを求めます（モジュラス）。
		4. この余りを 10 から引いたもの(10 - 余り)がチェックデジットです。余りが0の場合はチェックデジットも「0」になります。

	2	"モジュラス10 (ウェイト3・1)",
		1. 数値の各桁に、下の桁から３・１・３・１・…の順番に係数（ウェイト）を掛けます。
		2. 各桁の結果の合計を求めます。
		3. 合計を10で割り、余りを求めます（モジュラス）。
		4. この余りを 10 から引いたもの(10 - 余り)がチェックデジットです。余りが0の場合はチェックデジットも「0」になります。

	3	"モジュラス11 (ウェイト2～7)",
		1. 数値の各桁に、下の桁から２～７の係数（ウエイト）を掛けます。７の次はまた２に戻ります。
		2. 各桁の結果の合計を求めます。
		3. 合計を11で割り、余りを求めます（モジュラス）。
		4. この余りを 11 から引いたもの(11 - 余り)がチェックデジットです。余りが0または1の場合はチェックデジットも「0」になります。

	4	"モジュラス16",
	5	"モジュラス43",
	6	"モジュラス103",
	7	"セブンチェック 7DR (7DSR)",
		1. 数値を７で割り、余りを求めます。この余りが 7DR (Divide Remainder)と呼ばれるチェックデジットです。
		2. この余りを７から引いたもの(7 - 余り)が 7DSR(Divide Subtract Remainder) と呼ばれるチェックデジットです。

	9	"ナインチェック 9DR (9DRS)",
		1. 数値を9で割り、余りを求めます。この余りが 9DR と呼ばれるチェックデジットです。
		2. この余りを９から引いたもの(9 - 余り)が 9DSR と呼ばれるチェックデジットです。

	11	"法人番号",
		1. 数値の各桁に、下の桁から１・２・１・２・…の順番に係数（ウェイト）を掛けます。
		2. 各桁の結果の合計を求めます。
		3. 合計を９で割り、余りを求めます（モジュラス）。
		4. この余りを９から引いたもの(9 - 余り)がチェックデジットです。
		5. チェックデジットが先頭（左側）に付加する

	12	"ISBN-10"
		1. 数値の各桁に、 上の桁 から10～2の係数（ウエイト）を掛けます。
		2. 各桁の結果の合計を求めます。
		3. 合計を11で割り、余りを求めます（モジュラス）。
		4. この余りを 11 から引いたもの(11 - 余り)がチェックデジットです。
		   結果が11の場合にはチェックデジットは0、結果が10の場合にはチェックデジットはXになります。
*/
var
charList = {
	modulus16 : [	'0','1','2','3','4','5','6','7','8','9','-','$',':','/','・','+','A','B','C','D'],
	modulus43 : [	'0','1','2','3','4','5','6','7','8','9',
					'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q',
					'R','S','T','U','V','W','X','Y','Z','-','・','　','$','/','+','%']
}
;
var generator = function(){

	this.number     = '';
	this.type       = 0;
	this.weight     = '';
	this.weightList = [];
	this.checkdigit = 0;
	this.preview    = true;
}

generator.prototype.added = function(){

	var num   = this.number,
		digit = this.get()
	;

	switch(this.type){
	case 3:
		var numLen = num.length;
		return num.slice(0, numLen - 1) + digit + num.slice(numLen - 1, numLen);

	case 4:
		var code = num;
		if(num.match(/^\*.+\*$/))	code = num.replace('*', '');

		return '*' + code + digit + '*';

	case 5:
		return '';
	case 10:	return '' + digit + num;
	default:	return num + digit;
	}
}
generator.prototype.get = function(){

	var self = this;

	if(self.number == '')	return '';
	if(self.type < 10){
		if(self.weight == '')	return '';
	}

	self.weightList = (self.weight).replace(/[^0-9]/g, '').split('');

	switch(self.type){
	case 0:		return this.modulus10Split();
	case 1:		return this.modulus(10);
	case 2:		return this.modulus(11);
	case 3:		return this.modulus16();
	case 4:		return this.modulus43();
	case 5:		return this.modulus103();
	case 6:		return this.divideRemainder(7);
	case 7:		return this.divideSubtractRemainder(7);
	case 8:		return this.divideRemainder(9);
	case 9:		return this.divideSubtractRemainder(9);
	case 10:	return this.corporate();
	default:	return this.isbn()
	}
}
var sum  = function(arr) {
    return arr.reduce(function(prev, current, i, arr) {
        return prev + current;
    });
};
generator.prototype.modulus10Split = function(){

	var self = this,
		numList   = self.number.split(''),
		wNumber   = [numList.length - 1],
		revWeight = self.weightList.reverse(),
		modulus   = 10,
		remain    = 0,
		itemList  = [],
		sumItem   = 0
	;
	while (revWeight.length < self.number.length) {
		revWeight = revWeight.concat(revWeight);
	}
	numList.forEach(function(elm, i){

		wNumber[i]	= revWeight[i] * elm;
		if(modulus <= wNumber[i]){
			itemList = ('' + wNumber[i]).split('');
			sumItem = parseInt(itemList[0]) + parseInt(itemList[1]);
		}
	});

	remain	= sum(wNumber) % modulus;
	remain	= (remain == 0)? modulus : remain;
	return (modulus - remain);
}
generator.prototype.modulus = function(modulus){

	var self = this,
		numList   = self.number.split(''),
		wNumber   = [numList.length - 1],
		revWeight = self.weightList.reverse(),
		remain    = 0,
		itemList  = [],
		sumItem   = 0
	;
	while (revWeight.length < self.number.length) {
		revWeight = revWeight.concat(revWeight);
	}
	numList.forEach(function(elm, i){
		wNumber[i]	= revWeight[i] * elm;
	});
	remain	= sum(wNumber) % modulus;
	remain	= (remain == 0)? modulus : remain;
	return (modulus - remain);
}
generator.prototype.modulus16 = function(){

	var charList = charList.modulus16,
		numList  = this.number.split(''),
		total    = 0
	;
	numList.forEach(function(elm, i){
		total += charList.indexOf(elm);
	});
	return charList[16 - (total % 16)];
}
generator.prototype.modulus43 = function(){

	var charList = charList.modulus43,
		numList  = this.number.replace('*', '').split(''),
		total    = remain = 0,
	;
	numList.forEach(function(elm, i){
		total += charList.indexOf(elm);
	});
	if(total <= 43)	remain = total;
	else			remain = total % 43;

	return charList[remain];
}
generator.prototype.modulus103 = function(){


}

generator.prototype.divideRemainder = function(modulus){

	return parseInt(this.number) % modulus;
}
generator.prototype.divideSubtractRemainder = function(modulus){

	var remain =  parseInt(this.number) % modulus;
	return modulus - remain;
}
generator.prototype.corporate = function(){

	this.weightList = [1, 2];
	return this.modulus(9);
}
generator.prototype.isbn = function(){

	this.weightList = [2,3,4,5,6,7,8,9,10];
	return this.modulus(11);
}

module.exports = new generator();