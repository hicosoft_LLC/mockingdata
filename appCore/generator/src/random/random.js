/**
charTypeNumber
charTypeAlpha
charTypeSymbol
minNumber
maxNumber
minCharLength
maxCharLength 
digitAlign
charSize
symbolAscii
symbolAny
symbolItems
duplicate
*/
var generator = function(){
	
	this.parameter = {};
	this.dataList  = [];
	this.type      = 'String';
	this.preview   = true;
}
	
generator.prototype.exec = function() {	
	
	if(arguments.length == 1)	this.parameter = arguments[0];
		
	if(this.type === 'Number')	return this.randomNumber();
	if(this.type === 'String')	return this.randomString();
}

generator.prototype.randomNumber = function(){
		
	var self    = this,
		param	= self.parameter,
		list    = [],			
		padZero = '',
		randNum	= 0;
		randStr = '',
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	return new Promise(function(resolve, reject){
		
		if(param.digitAlign){
			
			padZero	= Array(param.charLength + 1).join('0');
			
			for(i = 0; i < genNumber; i++){
						
				randNum = Math.floor(Math.random() * (param.maxNumber + 1)) + param.minNumber;
				randStr = (padZero + randNum).slice(param.charLength * -1);
				list.push(randStr);
			}
		}
		else{
			for(i = 0; i < genNumber; i++){
						
				randNum = Math.floor(Math.random() * (param.maxNumber + 1)) + param.minNumber;
				list.push(randNum);
			}
		}
		resolve(list);
	});
}
	
generator.prototype.randomString = function(){
		
	var self    = this,
		param	= self.parameter,
		chars	= self.sources(),
		list    = [],
		str     = '',
		charLen = 1,
		counter = 0,
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	
	return new Promise(function(resolve, reject){
		
		if(chars.length == 0){
			resolve(list);
			return;
		}
		if(param.maxCharLength < 1){
			resolve(list);
			return;
		}
		
		if(param.digitAlign){
			if(param.duplicate){
				for(i = 0; i < genNumber; i++){
					
					str = '';
					for(j = 0; j < param.maxCharLength; j++){
						str += chars[Math.floor(Math.random() * chars.length)];
					}
					list.push(str);
				}				
			}
			else{
				while(counter < genNumber){
				
					str = '';
					for(j = 0; j < param.maxCharLength; j++){
						str += chars[Math.floor(Math.random() * chars.length)];
					}					
					if(list.indexOf(str) < 0){
						list.push(str);
						counter++;
					}
				}
			}
		}
		else{
			if(param.duplicate){
				for(i = 0; i < genNumber; i++){
					
					str = '';
					charLen = Math.floor(Math.random() * param.maxCharLength) + param.minCharLength;
					for(j = 0; j < charLen; j++){
						str += chars[Math.floor(Math.random() * chars.length)];
					}
					list.push(str);
				}
			}
			else{
				while(counter < genNumber){
					str = '';
					charLen = Math.floor(Math.random() * param.maxCharLength) + param.minCharLength;
					for(j = 0; j < charLen; j++){
						str += chars[Math.floor(Math.random() * chars.length)];
					}
					if(list.indexOf(str) < 0){
						list.push(str);
						counter++;
					}
				}
			}
		}
		resolve(list);
	});
}
	
generator.prototype.sources = function(){
		
	var self    = this,
		param	= self.parameter,
		chars	= []
	;
		
	if(param.charTypeSymbol && param.symbolAscii){
		Array.prototype.push.apply(chars, self.range('!', '~'));
		return chars;
	}
	if(param.charTypeNumber){
		Array.prototype.push.apply(chars, self.range('0', '9'));
	}
		
	if(param.charTypeAlpha){
		switch(param.charSize){
		case 0:		Array.prototype.push.apply(chars, self.range('A', 'Z'));	break;
		case 1: 	Array.prototype.push.apply(chars, self.range('a', 'z'));	break;
		default:	Array.prototype.push.apply(chars, self.range('A', 'Z'));
					Array.prototype.push.apply(chars, self.range('a', 'z'));	break;
		}			
	}
	if(param.symbolAny){
		if(param.symbolItems != ''){
			Array.prototype.push.apply(chars, param.symbolItems.split(""));
		}
	}
	return chars;
}
	
generator.prototype.range = function(fromChar, toChar){
		
	var min	= fromChar.charCodeAt(),
		max	= toChar.charCodeAt(),
		ary = []
	;
	for(i = min; i <= max; i++){
		ary.push(String.fromCharCode(i));
	}
	return ary;
}

module.exports = generator;
/*

文字の種類	ブロックの範囲（16進表記）	ブロック名
ひらがな	3040 ～ 309F	Hiragana
カタカナ	30A0 ～ 30FF	Katakana
漢字	4E00 ～ 9FFF	CJK Unified Ideographs
*/