/*
チェックデジットの種類
modulus10 分割
modulus10 一括
modulus10
modulus11
modulus16
modulus43
modulus103
セブンチェック 7DR
セブンチェック 7DSR
ナインチェック 9DR
ナインチェック 9DRS
法人番号
ISBN-10
*/
const
checkdigit = require('./checkdigit.js')

var generator = function(){

	this.checkdigit = {
		type : 0,
		weight : ''
	};
	this.dataList  = [];
	this.type      = 'String';
	this.preview   = true;
}

generator.prototype.create = function(){

	var self    = this,
		param	= self.parameter,
		list    = [],
		padZero = '',
		randNum	= 0;
		randStr = '',
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	return new Promise(function(resolve, reject){

		if(param.digitAlign){

			padZero	= Array(param.charLength + 1).join('0');

			for(i = 0; i < genNumber; i++){

				randNum = Math.floor(Math.random() * (param.maxNumber + 1)) + param.minNumber;
				randStr = (padZero + randNum).slice(param.charLength * -1);
				list.push(randStr);
			}
		}
		else{
			for(i = 0; i < genNumber; i++){

				randNum = Math.floor(Math.random() * (param.maxNumber + 1)) + param.minNumber;
				list.push(randNum);
			}
		}
		resolve(list);
	});
}

module.exports = generator;