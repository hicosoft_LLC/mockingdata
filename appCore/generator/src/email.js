/**
	(\W|^)[\w.+\-]{0,25}@(yahoo|hotmail|gmail)\.com(\W|$)
 	[0-9a-zA-Z_]{0,1}[0-9a-zA-Z_.+\-]{0,24}@[0-9a-zA-Z_\-]{3,63}.ドメイン$
 	[0-9a-zA-Z_]{1}[0-9a-zA-Z_.+\-]{0,24}@[0-9a-zA-Z_\-]{3,63}.ドメイン
  
 **/
const 
Q =  require('q'),
regex = require('./regex')
;

var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
}

generator.prototype.exec = function(){		
	
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var	self	= this,
		dbPath	= path.resolve(__dirname, '../../dbControl/mockingMaster.sqlite'),
		db		= new sqlite3.Database(dbPath),
		param	= self.parameter,
		list	= [],
		counter = 0,
		genRegex = new regex,
		strDomain = '',
		genNumber = (self.preview)? current.maxRows : current.dataCount
	;
	
	param.regexPattern	= param.mailPattern;
	
	return new Promise(function(resolve, reject) {

		genRegex.exec(param)
		.then(function(mails){
			return new Promise(function(resolveMail, rejectMail) {
				function addDomain() { 
					return new Promise(function(resolveDomain, rejectDomain) {		
										
						db.all(	
							appSrc.sql.domain,
							function(err, rows) {

								if(rows){
									$.each(rows, function(idx, elm){
										
										if(elm.isTop === 1)	strDomain	= elm.domainTopName;
										else{
											if(elm.organizationType === 'general-use'){
												strDomain	= elm.domainTopName;
											}
											else{
												strDomain	= elm.organizationType + '.' + elm.domainTopName;
											}
										}
										list.push(mails[counter] + '.' + strDomain);
										counter++;
									});
									resolveDomain();	
								}
							});
					})
					.then(function(){
						if(list.length < genNumber){
							addDomain();
						}	
						else{
							resolveMail(list);
						}
					});
				}
				addDomain();
			})
		})
		.then(function(res){
			db.close();
			genRegex	= null;
			resolve(res);
		})
	});
}

module.exports = generator;