/**
 *	infoType	: [zip, address]
 	addrItem	: [pref, city, town, number, building]
 	
 	郵便番号	:	zip3 (bit), 
 					zip4 (bit), 
 					zipSeparater (['区切り文字無し', 'ハイフン', 'スペース'])
 	
 	住所	:	addrSeparater	(['区切り文字無し', 'ハイフン', 'スペース'])
 				prefChar		(['都道府県コード', '漢字', 'ひらがな', '全角カタカナ', '半角カタカナ']
 								 [prefCode, prefName, prefKanaH, prefKanaK, prefKanaHK]
 								)
 				cityChar		(['市区町村コード', '漢字', 'ひらがな', '全角カタカナ', '半角カタカナ']
 								 [cityCode, cityNama, cityKanaH, cityKanaK, cityKanaHK]
 								)
 				townChar		(['漢字', 'ひらがな', '全角カタカナ', '半角カタカナ']
 								 [townName, townKanaH, townKanaK, townKanaHK]
 								)
 				numberChar		(['半角', '全角', '全半角混合'])
 				buildingChar	(['漢字', 'ひらがな', '全角カタカナ', '半角カタカナ']
 								 [buildingName, buildingKanaH, buildingKanaK, buildingKanaHK]
 								)
 **/
 /**
 	処理内容と手順
 	1.	DataCount分のデータObjectがすでにあるかチェック
 	2.	ない場合、DataCount分の住所データと建物データを取得
 **/
var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
	this.genNumber = 100;
}

generator.prototype.exec = function(){
		
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var self    = this,
		param	= self.parameter,
		colMaster = appSrc.master.columnName.address,
		colList	= []
	;
	self.genNumber = (self.preview)? current.maxRows : current.dataCount;
		
	if(param.infoType == 'zip'){
		if(param.zip3)		colList.push('zip3');
		if(param.zip4)		colList.push('zip4');
	}
	if(param.infoType == 'address'){
			
		if(-1 < param.addrItem.indexOf('pref'))		colList.push(colMaster.pref[param.prefChar]);
		if(-1 < param.addrItem.indexOf('city'))		colList.push(colMaster.city[param.cityChar]);
		if(-1 < param.addrItem.indexOf('town'))		colList.push(colMaster.town[param.townChar]);
		if(-1 < param.addrItem.indexOf('number'))	colList.push(colMaster.pref[param.numberChar]);
		if(-1 < param.addrItem.indexOf('building'))	colList.push(colMaster.building[param.buildingChar]);
	}
	
	console.log(colList);
	
	if(!self.hasGeneratedAddressData()){
		
	}
	if(!self.hasGeneratedBuildingData()){
		
	}
	
	
	return Promise.resolve();
}
generator.prototype.hasGeneratedAddressData = function(){
	
	if(!Array.isArray(delegate.address))			return false;
	if(delegate.address.length < current.dataCount)	return false;
	return true;
}
generator.prototype.hasGeneratedBuildingData = function(){
	
	if(!Array.isArray(delegate.building))				return false;
	if(delegate.building.length < current.dataCount)	return false;
	return true;
}
generator.prototype.zipList = function(){	
	
	var self    = this,
		param	= self.parameter
	;
	return new Promise(function(resolve, reject){
		console.log('zipList');
		resolve([]);
	});
}

generator.prototype.buildingList = function(){
	
	var self    = this,
		param	= self.parameter
	;
	var promise = new Promise(function(resolve, reject){
		resolve([]);
	});
	return promise;
}

module.exports = generator;