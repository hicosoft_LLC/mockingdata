/**
 * 設定したパターンからデータを生成し、指定ファイルへ保存
 **/
const 
sleep 		= require('sleep'),
address		= require('./address'),
anything	= require('./anything'),
blank	    = require('./blank'),
column		= require('./column'),
encryption	= require('./encryption'),
database	= require('./database'),
datetime	= require('./datetime'),
delegate    = require('./delegate'),
email		= require('./email'),
increment	= require('./increment'),
longtext	= require('./longtext'),
person		= require('./person'),
phone		= require('./phone'),
random		= require('./random'),
regex		= require('./regex')
;

var generator = function(){
	this.parameter      = {},
	this.dataList       = [],
	this.generatorList	= {},
	this.preview        = false,
	this.saveOptions    = {
		encoding : 'utf8',
		mode     : 0o666,
		flag     : 'w+'
	}
}
/*
function puts(str) {
  return new Promise(function(resolve) {
    setTimeout(function() {
      resolve(str);
    }, 1000);
  });
}


var promises = [
  puts(1),
  puts(2),
  puts(3)
];

// 並列処理に対してPromise.allを使う
Promise.all(promises).then(function(results) {
  console.log(results);
});

## 複数の非同期処理が全て終わった -----------------
$.when(
    $.getJSON('a.json'),
    $.getJSON('b.json')
)
then(function(){
	
}).
.done(function(data_a, data_b) {
    // すべて成功した時の処理
    console.log(data_a, data_b);
})
.fail(function() {
    // エラーがあった時
    console.log('error');
});
*/
generator.prototype.exec = function(){
	
	if(arguments.length == 1)	this.parameter = arguments[0];
	var self = this,
		param= self.parameter,
		elmExport	= $('.exporting'),
		elmCreating	= $('.creating', elmExport),
		elmSaving	= $('.saving', elmExport)
	;
	self.dataList	= new Array(param.patternList.length);
	self.generatorList	= self.getGenerator();
	
	if(self.preview)	return self.createDataList();

	self.createDataList()
	.then(function(dataList){
		switch(param.export.type){
		case 'csv':		return self.toCsv(dataList);
		case 'xml':		return self.toXml(dataList);
		case 'json':	return self.toJson(dataList);
		case 'json5':	return self.toJson5(dataList);
		default:		return self.toDsv(dataList);
		}
	});
}

generator.prototype.getGenerator = function(){
		
	var self	= this,
		patternList	= current.patternList,
		genList = {
			single : new Array(patternList.length),
			relative : new Array(patternList.length)
		}
		genItem	= {},
		pattern	= {},
		genObj	= null
	;
	for(var i = 0; i < patternList.length; i++){
		
		genObj  = null;
		pattern = patternList[i];
		
		switch(pattern.type){
		case 'increment':
			genObj	= new increment;	
			break;
	
		case 'randomNumber':
			genObj	= new random;			
			genObj.type	= 'Number';
			break;
			
		case 'randomString':
			genObj	= new random;			
			genObj.type	= 'String';
			break;
			
		case 'regex':	
			genObj	= new regex;			
			break;
		
		case 'encryption':	
			genObj	= new encryption;			
			break;
			
		case 'anything':	
			genObj	= new anything;
			break;
			
		case 'person':		
			genObj	= new person;	
			break;
			
		case 'address':		
			genObj	= new address;	
			break;
			
		case 'email':		
			genObj	= new email;		
			break;
			
		case 'phone':		
			genObj	= new phone;		
			break;
			
		case 'datetime':	
			genObj	= new datetime;	
			break;
			
		case 'longtext':	
			genObj	= new longtext;	
			break;	
					
		default:	
			genObj	= new blank;					
			break;
		}
		genObj.parameter= pattern.param;
		genObj.preview	= self.preview;
		
		if(('sourceColumnIndex' in pattern.param)){
			genList.single[i]	= null;
			genList.relative[i]	= genObj;
		}
		else{
			genList.single[i]	= genObj;
			genList.relative[i]	= null;
		}
	//	console.log(genList);	
	}
	return genList;
}
	
generator.prototype.createDataList = function(){
		
	var self	= this,
		ptnList	= self.parameter.patternList,
		singleList = new Array(ptnList.length),
		relativeList = new Array(ptnList.length),
		genList = []
	;
	
	$.each(self.generatorList.single, function(i, obj){
		singleList[i] = new Promise(function(resolve, reject){
			if(obj)		resolve(obj.exec());
			else		resolve([]);
		})
	});
	
	return new Promise(function(singleResolve, reject){
	                          
		Promise.all(singleList)
		.then(function(singleRes) {
			
			$.each(self.generatorList.relative, function(i, obj){
				relativeList[i] = new Promise(function(resolve, reject){
					if(obj){
						obj.parentDataList = singleRes[obj.parameter.sourceColumnIndex];
						resolve(obj.exec());
					}
					else{
						resolve([]);
					}
				})
			});
			
			Promise.all(relativeList)
			.then(function(relativeRes) {
				$.each(relativeRes, function(i, elm){					
					if(0 < elm.length)	singleRes[i] = elm;
				});
				singleResolve(singleRes);
			});
		});
	});
}

generator.prototype.toCsv = function(dataList){
	
	var self	= this,
		param	= self.parameter,
		rowItems = [],
		rows = [],
		rate = 0,
		w = '',
		pgBar	= $('.progress-bar')
	;
	for(var i = 0; i < param.dataCount; i++){
		rowItems	= [];
		for(var j = 0; j < param.patternList.length; j++){
			rowItems.push(dataList[j][i]);
		}
		rows.push(rowItems.join(','));

		rate	= (i / param.dataCount) * 100;
		
		if(Math.round(rate) === rate){
			w = 'width:' + rate + '%';
			pgBar.prop('style', w);
		}			
	}
	fs.writeFile(	param.export.savePath, 
					rows.join('\n'), 
					self.saveOptions,
					(e) => {
						if(e)	console.error(e);
						else	return;//	append.modal.close();
					});
}
generator.prototype.toXml = function(){
		
}
generator.prototype.toJson = function(){
		
}
generator.prototype.toJson5 = function(){
		
}
generator.prototype.toDsv = function(){
		
}

module.exports = generator;