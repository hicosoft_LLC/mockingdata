/**
 *
 **/
var generator = function(){
	this.parameter = {};
	this.dataList  = [];
	this.preview   = true;
}

generator.prototype.exec = function() {	
	
	if(arguments.length == 1)	this.parameter = arguments[0];

	var self    = this,
		param	= self.parameter,
		stepNo	= param.stepNo,
		startNo	= param.startNo,
		padding	= param.padding,
		digit	= (param.digit == '')? 0 : param.digit,
		padStr  = padding ? Array(digit + 1).join('0') : '',
		genNo   = (self.preview)? current.maxRows : current.dataCount,
		iVal    = 0;
		sVal    = '';
		list    = []
	;	
	return new Promise(function(resolve, reject){
		
		if(padding){
			for(i = 0; i < genNo; i++){				
				iVal = (i * stepNo) + startNo;				
				sVal = (padStr + iVal).slice(digit * -1);				
				list.push(sVal);
			}
		}
		else{
			for(i = 0; i < genNo; i++){
				list.push((i * stepNo) + startNo);
			}
		}
		self.dataList = list;
		resolve(list);
	});
}

module.exports = generator;