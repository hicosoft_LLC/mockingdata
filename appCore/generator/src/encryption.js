/**
 *	
	 targetColumn		: 暗号化対象column.index
	 targetFilePath		: 暗号化対象ファイルパス
	 cryptoAlgorithm	: ['MD4','MD5','MDC2','SHA','SHA1','SHA224','SHA256','SHA384','SHA512']
	 outEncoding		: ['HEX', 'BASE64']

-- [ crypto ]でサポートされているハッシュアルゴリズム(OpenSSLに依存)
openssl [	list-standard-commands 
		|	list-message-digest-commands 
		|	list-cipher-commands 
		|	list-cipher-algorithms 
		|	list-message-digest-algorithms 
		|	list-public-key-algorithms]
       
DSA
DSA-SHA
DSA-SHA1
DSA-SHA1-old
RSA-MD4
RSA-MD5
RSA-MDC2
RSA-RIPEMD160
RSA-SHA
RSA-SHA1
RSA-SHA1-2
RSA-SHA224
RSA-SHA256
RSA-SHA384
RSA-SHA512
dsaEncryption
dsaWithSHA
dsaWithSHA1
dss1
ecdsa-with-SHA1
md4
md4WithRSAEncryption
md5
md5WithRSAEncryption
mdc2
mdc2WithRSA
ripemd
ripemd160
ripemd160WithRSA
rmd160
sha
sha1
sha1WithRSAEncryption
sha224
sha224WithRSAEncryption
sha256
sha256WithRSAEncryption
sha384
sha384WithRSAEncryption
sha512
sha512WithRSAEncryption
shaWithRSAEncryption
ssl2-md5
ssl3-md5
ssl3-sha1
whirlpool
 **/
const crypto = require('crypto');

var generator = function(){
	this.parameter = {};
	this.parentDataList = [];
	this.dataList  = [];
	this.preview   = true;
}
generator.prototype.exec = function(){
	
	if(arguments.length == 1)	this.parameter = arguments[0];
	
	var self      = this,
		param     = self.parameter,
		master    = appSrc.master.seletOptions.crypto,
		algorithm = master.algorithm[param.cryptoAlgorithm].toLowerCase(),
		encType   = master.encoding[param.outEncoding].toLowerCase(),
		hash      = ''
	;

	return new Promise(function(resolve, reject){
		
		if(param.criptoSource == 'column'){
			
		//	var list = self.sourceInGrid(param.targetColumn);
			
			$.each(self.parentDataList, function(i, elm){
							
				hash =	crypto.createHash(algorithm)
						.update(elm)
						.digest(encType);

				self.dataList.push(hash);
			});
			resolve(self.dataList);
		}
		if(param.criptoSource == 'file'){
			
			var fileBody = fs.readFileSync(param.targetFilePath, 'utf8'),
				srcItems = fileBody.split('\n'),
				item     = ''
			;
			$.each(srcItems, function(i, elm){
				
				item = $.trim(elm);
				hash = '';
				
				if(item != ''){
					hash =	crypto.createHash(algorithm)
							.update(item)
							.digest(encType);
				}
				self.dataList.push(hash);
			});
			resolve(self.dataList);
		}
		resolve([]);
	});
}
generator.prototype.sourceInGrid = function(iColumn){
	
	var tdList = $('#confirmGrid tbody tr td:nth-child(' + (iColumn + 1) + ')')
		list   = []
	;
			
	$.each(tdList, function(i, elm){
		list.push($(this).text());
	});
	this.parentDataList	= list;
	return list;
}

module.exports = generator;