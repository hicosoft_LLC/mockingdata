/**
 *
 **/
module.exports = {
	
	tableRow : function(dataList){
		var tmpTag = '',
			tags = []
		;
		$.each(dataList, function(i, elm){
			
		})
	},
	selectOptions : function(list, addBlank){
		
		var tmpTag	= '<option value="{0}">{1}</option>',
			tags	= []
		;
		if(addBlank)	tags.push(to.string.format(tmpTag, '', ''));
		
		$.each(list, function(idx, elm){
			tags.push(to.string.format(tmpTag, idx, elm));
		});
		
		return tags;
	},
	comboBox : function(list){
		
		var tmpTag	= '<li idx="{0}">{1}</li>',
			tags	= []
		;		
		$.each(list, function(idx, elm){
			tags.push(to.string.format(tmpTag, idx, elm));
		});
		return tags;
	},
	sideMenu : function(pj){
		
		var tmpGrpItem	= '<div>{0}</div>',
			tmpPtnItem	= '<div fileName="{0}">{1}</div>',
			tmpGrp  = '<nav class="setpattern-list">{0}</nav>'
			list	= [],
			grpList = []
		;

		$.each(pj, function(i, category){
			
			list.push(to.string.format(tmpGrpItem, i));
			
			if(typeof(category) == 'object'){
				
				grpList	= [];
				
				$.each(category, function(j, group){
					grpList.push(to.string.format(tmpPtnItem, group, j));
				});
				list.push(to.string.format(tmpGrp, grpList.join('\n')));
			}
		});		
		return list;
	}
}