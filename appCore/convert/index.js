/**
 *
 **/
const
locateName = ['ja', 'en'],
monthName = {
	long : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	short: ['Jan.','Feb.','Mar.','Apr.','May.','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.']
},
weekName = {
	ja : {
		long : ['日','月','火','水','木','金','土'],
		short: ['日','月','火','水','木','金','土']
	},
	en : {
		long : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
		short: ['Sun.','Mon.','Tue.','Wed.','Thu.','Fri.','Sat.']
	}
}
;
			
module.exports = {

	string : {
		format : function(){

			if(arguments.length == 0)	return '';
			if(arguments.length == 1)	return arguments[0];

			var s = arguments[0],
				a = arguments[1],
				reg = null
			;
			if(Array.isArray(a)){
				for(var i = 0; i < a.length; i++){
					reg = new RegExp("\\{" + (i) + "\\}", "gm");
					s = s.replace(reg, arguments[i]);
				}
				return s;
			}
			if(a instanceof Object && !(a instanceof Array)){
				Object.keys(a).forEach(function(key){
					reg = new RegExp("\\{" + key + "\\}", "gm");
					s = s.replace(reg, this[key]);
				});
				return s;
			}
			for(var i = 1; i < arguments.length; i++) {
				reg = new RegExp("\\{" + (i - 1) + "\\}", "gm");
				s = s.replace(reg, arguments[i]);
			}
			return s;
		},
		toInline : (s)=>{
			return $.trim(s.replace("\t"," ").replace(/[\n\r]/g," "));
		}
	},
	datetime : function(format, d, location){
		
		var	osLang	= app.getLocale(),
			langs   = ['ja', 'en']
			lang	= (osLang == 'ja')?	'ja' : 'en',
			locale  = location === undefined ? lang : location
		;
			
		return	format	.replace('yyyy', d.getFullYear())
						.replace('yy',   ('' + d.getFullYear()).substr(2))
						.replace('mm',   this.toDouble(d.getMonth() + 1))
						.replace('m',    d.getMonth() + 1)
						.replace('dd',   this.toDouble(d.getDate()))
						.replace('d',    d.getDate())
						.replace('hh',   this.toDouble(d.getHours()))
						.replace('h',    d.getHours())
						.replace('nn',   this.toDouble(d.getMinutes()))
						.replace('n',    d.getMinutes())
						.replace('ss',   this.toDouble(d.getSeconds()))
						.replace('s',    d.getSeconds())
						.replace('ff',   this.toTriple(d.getMilliseconds()))
						.replace('f',    d.getMilliseconds())
						.replace('MM',   this.getMonthName(d.getMonth(), locale, 'long'))
						.replace('M',    this.getMonthName(d.getMonth(), locale, 'short'))
						.replace('ww',   weekName[locale]['long'][d.getDay()])
						.replace('w',    weekName[locale]['short'][d.getDay()])
						;						
	},
	now : function(format){
		return this.datetime(format, new Date());
	},
	today : function(format){
		return this.date(format, new Date());
	},
	date : function(format, d){
		return	format	.replace('yyyy', d.getFullYear())
						.replace('yy',   ('' + d.getFullYear()).substr(2))
						.replace('MM',   this.toDouble(d.getMonth() + 1))
						.replace('M',    d.getMonth() + 1)
						.replace('dd',   this.toDouble(d.getDate()))
						.replace('d',    d.getDate());
	},
	getMonthName : function(month, locale, lenType){
		
		if(locale == 'ja')	return month + 1;
		return monthName[lenType][month];
	},
	toDouble : function(num){
		var newer = '' + num;
		if (newer.length < 2) {
			newer = "0" + newer;
		}
		return newer;
	},
	toTriple : function(num){
		var newer = '' + num;
		if (newer.length < 3) {
			newer = "00" + newer;
		}
		return newer.slice(-3);
	}
}