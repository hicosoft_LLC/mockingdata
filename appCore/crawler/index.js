/*
 *
 */
const
cmdExe	= require('child_process').exec
;

var crawler = function(){
	this.baseURL = 'http://www.mapion.co.jp/address/';
	this.saveRoot = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/{0}';
}

crawler.prototype.getPref = function(){

	var	self	= this,
		stt	= to.string.format("curl {0} | sed -n -E '/<li.+href.+\\/address\\/[0-9]{1,}/p'", self.baseURL)
	;
	var promise = new Promise(function(resolve, reject) {
		var child = cmdExe(stt, function (error, stdout, stderr) {

			var lines	=	stdout.split('\n'),
				item	= '',
				replaced= []
			;
			$.each(lines, function(i, elm){

				item	= elm
			              .replace(/^.+(href="\/address\/)/g, '')
			              .replace(/(<\/a><\/li>)/g, '')
			              .replace(/(title).+>/g, '')
			              .replace(/"|\//g, '')
			              .replace(/\s+/g, ',');

				if(item != '')	replaced.push(item);
			});
			fs.writeFileSync(to.string.format(self.saveRoot, 'pref.txt'), replaced.join('\n'), 'utf-8');
			resolve(replaced);
		});
	});

	return promise;
}
crawler.prototype.getCity = function(prefList){

	var	self	= this;

	var promise = new Promise(function(resolve, reject) {

		prefList.reduce(function(prev, current, i, ary){

			var promise = new Promise(function(resolve, reject){

				var replaced	= [];
				var prefItem	= current.split(',');
				var statement	= to.string.format(`curl {0}{1}/ | grep -A 10000 \'<dl class="list-a-z">\' | sed -n -E \'/<span class="rt">.+|<a.+href.+\\/address\\/[0-9]{1,}/p\'`, self.baseURL, prefItem[0]);

				var child = cmdExe(statement, function (error, stdout, stderr) {
					var obj = {
						cityDir  : to.string.format(self.saveRoot, 'pref/' + prefItem[0]),
						savePath : to.string.format(self.saveRoot, 'pref/' + prefItem[0] + '/city.txt'),
						list     : stdout
					}
					resolve(obj);
				});
			}).then(function(res){

				var lines	= res.list.split('\n'),
					line	= '',
					lineItem= [],
					item	= '',
					nameCode= [],
					replaced= []
				;

				for(var j = 0; j < lines.length; j++){

					line	= $.trim(lines[j]);
					if(line.match(/<span/)){
						lineItem= [],
						item	= line
								.replace(/<span class="rt">/, '')
								.replace(/<\/span>/, '')
								.replace(/\）/, '');

						if(item.match(/（/)){
							item	= item.replace(/\（/, ',');
							lineItem.push(item);
						}
						else{
							lineItem.push(item);
							lineItem.push('');
						}
						continue;
					}
					if(line.match(/<a/)){
						//	<a href="/address/11245/" title="ふじみ野市">ふじみ野市</a>
						item	= line
								.replace(/<a href="\/address\//, '')
								.replace(/".+">/, '')
								.replace(/<\/a>/, '')
								.replace(/\）/, '');

						nameCode = item.split('/');
						lineItem.push(nameCode[0]);

						if(nameCode[1].match(/（/)){
							nameCode[1]	= nameCode[1].replace(/\（/, ',');
							lineItem.push(nameCode[1]);
						}
						else{
							lineItem.push(nameCode[1]);
							lineItem.push('');
						}

						replaced.push(lineItem);
						continue;
					}
				};
				fs.mkdir(res.cityDir, function(){
					fs.writeFile(res.savePath, replaced.join('\n'), (err) => {
						if (err) throw err;
						console.log(' " It\'s saved!');
					});
				});
			});
		}, 0);
		resolve();
	});
	return promise;
}
crawler.prototype.getTown = function(prefCode){

	var	self	= this
		prefPath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/',
		prefDirs = fs.readdirSync(prefPath),
		prefDir  = prefCode, //prefDirs[24],
		fileList = []
	;

	var dirCity = prefPath + prefDir;
	var stat = fs.statSync(dirCity);

	if(stat.isDirectory()){

		var cityPath = dirCity + '/converted/city.txt';
		var fileBody = fs.readFileSync(cityPath, 'utf-8');

		var obj = {
			path : dirCity,
			parentDir : prefDir,
			cityCode : fileBody.match(/[0-9]{1,}/g)
		}
		fileList.push(obj);
	}

	var promise = new Promise(function(resolve, reject) {

		fileList.reduce(function(prev, current, i, ary){

			var parentDir = current.parentDir;
			var promise = new Promise(function(resolve, reject){

				current.cityCode.reduce(function(prev, city, j, ary){

					var promise = new Promise(function(resolve, reject){

						var replaced	= [];
						var prefItem	= city;
						var statement	= to.string.format(`curl {0}{1}/ | grep -A 10000 \'<dl class="list-a-z">\' | sed -n -E \'/<span class="rb">.+|<span class="rt">.+/p\'`, self.baseURL, city);

						var child = cmdExe(statement, function (error, stdout, stderr) {
							var obj = {
								townDir  : to.string.format(self.saveRoot, 'pref/' + parentDir),
								savePath : to.string.format(self.saveRoot, 'pref/' + parentDir + '/' + city + '.csv'),
								list     : stdout,
								city     : city
							}
							resolve(obj);
						});
					}).then(function(res){

						var lines	= res.list.split('\n'),
							line	= '',
							lineItem= [],
							item	= '',
							nameCode= [],
							replaced= []
						;
						for(var j = 0; j < lines.length; j++){

							line	= $.trim(lines[j]);

							if(line.match(/class="rt"/)){
								item	= line
										.replace(/<span class="rt">/, '')
										.replace(/<\/span>/, '')
										.replace(/\）/, '');

								if(item.match(/（/)){
									item	= item.replace(/\（/, ',');
									lineItem.push(item);
								}
								else{
									lineItem.push(item);
									lineItem.push('');
								}
								continue;
							}
							if(line.match(/class="rb"/)){
								//	<span class="rb"><a href="/address/07208/1953/" title="福島県喜多方市山都町早稲谷千仏屋敷">山都町早稲谷（千仏屋敷）</a></span>
								item	= line
										.replace(/^.+<a href="\/address\//, '')
										.replace(/".+">/, '')
										.replace(/<\/a>/, '')
										.replace(/<\/span>/, '')
										.replace(/\）/, '');

								nameCode = item.split('/');
								lineItem.push(nameCode[0]);
								lineItem.push(nameCode[1]);

								if(nameCode[2].match(/\（/)){
									nameCode[2]	= nameCode[2].replace(/\（/, ',');
									lineItem.push(nameCode[2]);
								}
								else{
									lineItem.push(nameCode[2]);
									lineItem.push('');
								}
								replaced.push(lineItem);
								lineItem	= [];
								continue;
							}
						};
					//	console.log(res);
						fs.writeFile(res.savePath, replaced.join('\n') + '\n', (err) => {
							if (err) throw err;
							console.log(res.city + ' : saved!');
						});
					});
				}, 0);
			});
		}, 0);
	});

	return promise;
}
crawler.prototype.createTownDir = function(){

	var prefPath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/',
		prefDirs = fs.readdirSync(prefPath)
	;
	prefDirs.reduce(function(prev, prefCode, i, ary){

		var dirCity = prefPath + prefCode;
		var stat = fs.statSync(dirCity);
		var tgtDirName = 'town--ban-gou';
		var tgtDir = dirCity + '/' + tgtDirName;

	//	if(stat.isDirectory()){

		//	chomeFiles = fs.readdirSync(dirCity + '/town-chome-ban-gou/');

		//	chomeFiles.reduce(function(prev, csvName, j, ary){
		//	for(var j = 0; j < chomeFiles.length; j++){

			//	if(csvName.match(/csv$/)){
			//	var chomeFile = chomeFiles[j];
			//	if(!fs.existsSync(tgtDir))	fs.mkdirSync(tgtDir);

				//	console.log(csvName);
				//	var statement	= to.string.format('cat {0}{1}/*.csv > {0}{1}/town--ban-gou.txt', prefPath, prefCode);
				//	var statement	= to.string.format('mv {0}{1}/*.csv  {0}{1}/{2}/', prefPath, prefCode, tgtDirName);
				//	var statement	= to.string.format('mv {0}{1}/chome-ban-gou  {0}{1}/town-chome-ban', prefPath, prefCode);
				//	var statement	= to.string.format("rm '{0}{1}/chome/*.csv'", prefPath, prefCode);
				//	var statement	= to.string.format(' cat {0}{1}/chome.txt | sed -n -E \'/[0-9]{1,}::[0-9]{1,}/p\' > {0}{1}/ban.txt', prefPath, prefCode);
				//	console.log(statement);
				//	var child = cmdExe(statement, function (err, stdout, stderr) {
				//		if(err)	console.error(err);
				//		if(stdout)	console.log(stdout);
				//	});
			//	}
		//	}, 0);
		//	return;
		//	}

		/*
			var mv	= to.string.format('mv {0}{1}/*.csv {2}', prefPath, prefDirs[i], townPath);

			var child = cmdExe(mv, function (err, stdout, stderr) {
				if(err)	console.error(err);
				if(stdout)	console.log(stdout);
			});
		*/
	//	}
	}, 0);
}
crawler.prototype.getChome = function(pref, idxFrom, idxTo){

	var	self	= this;
	var prefPath    = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/',
		tmpSavePath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/{0}/{1}-{2}.csv';
		prefCode    = pref,
		townTxt     = fs.readFileSync(prefPath + prefCode + '/town.txt', 'utf-8'),
		townList    = townTxt.match(/[0-9]{5},[:0-9]{1,}/g),
		townSet     = [],
		townRange	= [],
		counter		= 0
	;
	if(townList == null){
		console.log(pref + 'は終了しました。');
		return Promise.resolve(true);
	}
	for(var i = 0; i < townList.length; i++){
		townSet.push(townList[i].split(','));
	}

	$('.sourceData').text(townSet.length);

	if(idxFrom == ''){
		townRange	= townSet;
	}
	else{
	//	counter	= idxFrom;
		if(idxTo == '')	townRange	= townSet.slice(idxFrom);
		else			townRange	= townSet.slice(idxFrom, idxTo);
	}
	console.log(townRange.length);

	var promise = new Promise(function(resolve, reject) {

		townRange.reduce(function(prev, current, i, ary){

			var promise = new Promise(function(resolve, reject){
				//<li class="list-8"><a href="/address/04203/16::1/" title="宮城県塩竈市浦戸石浜金ケ浜50">50</a></li>
				var statement	= to.string.format(`curl {0}{1}/{2}/ | sed -n -E '/<a.+href.+\\/address\\/[0-9]{5}\\/[0-9]{1,}:[0-9:]{1,}/p\'`, self.baseURL, current[0], current[1]);

				var child = cmdExe(statement, {maxBuffer: 400*1024}, function (error, stdout, stderr) {

					if(stdout.length === 0){
						var obj = {
							tgtPath  : prefPath + prefCode + '/town.txt',
							fileName : current[0] + '-' + current[1],
							savePath : to.string.format(tmpSavePath, prefCode, current[0], current[1]),
							list     : ''
						}
						resolve(obj);
					}
					if(stdout){
					//	console.log(stdout.length);
						var obj = {
							tgtPath  : prefPath + prefCode + '/town.txt',
							fileName : current[0] + '-' + current[1],
							savePath : to.string.format(tmpSavePath, prefCode, current[0], current[1]),
							list     : stdout
						}
						resolve(obj);
					}
				//	if(stderr){
				//		console.log(stderr);
				//	}
					if(error){
						console.error(current[0] + '-' + current[1] + ' : ' + error);
						var obj = {
							tgtPath  : prefPath + prefCode + '/town.txt',
							fileName : current[0] + '-' + current[1],
							savePath : to.string.format(tmpSavePath, prefCode, current[0], current[1]),
							list     : ''
						}
						reject(obj);
					}
				});
				child= null;
			}).then(function(res){

				var lines	= res.list.split('\n'),
					line	= '',
					lineItem= [],
					item	= '',
					nameCode= [],
					replaced= []
				;

				for(var j = 0; j < lines.length; j++){

					line	= $.trim(lines[j]);
					lineItem= [];

					if(line.match(/<a/)){
						//<li class="list-8"><a href="/address/11222/5::48/" title="埼玉県越谷市大沢659">659</a></li>
						item	= line
								.replace(/^.+<a href="\/address\//, '')
								.replace(/<\/a><\/li>/, '')
								.replace(/".+>/, '');

						nameCode = item.split('/');
						lineItem.push(nameCode[0]);
						lineItem.push(nameCode[1]);
						lineItem.push(nameCode[2]);

						replaced.push(lineItem);
					//	continue;
					}
				};
			//	console.log(res);
			//	console.log(replaced.length);
			//	resolve(replaced);
				fs.writeFile(res.savePath, replaced.join('\n') + '\n', (err) => {
					if(err){
						console.error(res.fileName + ' : ' + err);
						return;
					}


				//	console.log(res.fileName + ' : saved !');
					var state = "sed -i -E '/" + res.fileName.replace('-', ',') + "/d' '" + res.tgtPath + "'";

					cmdExe(state, function (error, stdout, stderr) {

						counter++;
					//	if(stdout){
					//		console.log(res.fileName + ' : saved !');
					//		resolve(true);
					//	}
						if(error){
							console.error(counter + ', ' + res.fileName + ' : error');
					//		reject();
						}
						else{
							console.log(counter + ', ' + res.fileName + ' : saved !');
						}
					});

				});
			});
		}, 0);
		resolve();
	});
	return promise;
}
crawler.prototype.getBanchi = function(prefCode, idxFrom, idxTo){


	//05201,100:2:15,15
	//<li class="list-8"><a href="/address/05201/100:2:15:1/" title="秋田県秋田市飯島道東2丁目15-2">2</a></li>
	var	self	= this;
	var prefPath    = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/',
		tmpSavePath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/{0}/{1}.csv';
		choTxt     = fs.readFileSync(prefPath + prefCode + '/importFiles/chome-kana.csv', 'utf-8'),
		choList    = choTxt.split('\n'),//choTxt.match(/[0-9]{5},[0-9]{1,}::[0-9]{1,}/g),
		choSet     = [],
		choRange   = [],
		tmpCmd     = "curl {0}{1}/{2}:{3}/ | sed -n -E '/<a.+href.+\\/address\\/[0-9]{5}\\/[0-9]{1,}:[0-9]{0,}:[0-9]{0,}/p'"
	;

	for(var i = 0; i < choList.length; i++){
		choSet.push(choList[i].split(','));
	}

	if(idxFrom == ''){
		choRange	= choSet;
	}
	else{
		if(idxTo == '')	choRange	= choSet.slice(idxFrom);
		else			choRange	= choSet.slice(idxFrom, idxTo);
	}

	var promise = new Promise(function(resolve, reject) {

		choRange.reduce(function(prev, current, i, ary){

			var promise = new Promise(function(resolve, reject){

				//01,01202,155,3,３丁目,３ちょうめ,３チョウメ,3ﾁｮｳﾒ
				//http://www.mapion.co.jp/address/01202/101:1/

				//<li class="list-8"><a href="/address/01202/1::10:1/" title="北海道函館市青柳町10-1">1</a></li>
				var statement	= to.string.format(tmpCmd,
				                                 self.baseURL,
				                                 current[1],
				                                 current[2],
				                                 current[3]);

			//	console.log(statement);
			//	return true;

				var child = cmdExe(statement, function (error, stdout, stderr) {

				//	console.log(current[2] + '-' + current[3]);
				//	if(error)	console.error(error);
				//	if(stderr)	console.log(stderr);
					if(stdout){
						var fileName = current[1] + '-' + current[2] + '-' + current[3];
						var obj = {
							fileName : fileName,
							savePath : to.string.format(tmpSavePath, prefCode, fileName),
							list     : stdout
						}
					//	console.log(stdout);
						resolve(obj);
					}
				});
			})
			.then(function(res){

				var lines	= res.list.split('\n'),
					line	= '',
					lineItem= [],
					item	= '',
					nameCode= [],
					replaced= []
				;

				for(var j = 0; j < lines.length; j++){

					line	= $.trim(lines[j]);
					lineItem= [];

					if(line.match(/<a/)){
						//<li class="list-8"><a href="/address/05201/100:2:15:1/" title="秋田県秋田市飯島道東2丁目15-2">2</a></li>
						item	= line
								.replace(/^.+<a href="\/address\//, '')
								.replace(/<\/a><\/li>/, '')
								.replace(/".+>/, '');

						nameCode = item.split('/');
						lineItem.push(nameCode[0]);
						lineItem.push(nameCode[1]);
						lineItem.push(nameCode[2]);

						replaced.push(lineItem);
					}
				};
			//	console.log(res.fileName);
			//	resolve(replaced);
				fs.writeFile(res.savePath, replaced.join('\n') + '\n', (err) => {
					if (err) throw err;
					console.log(res.fileName + ' : saved!');
				});
			});
		}, 0);
	});
	return promise;
}
crawler.prototype.getGou = function(prefCode, idxFrom, idxTo){

	//05201,100:2:15,15
	//<li class="list-8"><a href="/address/05201/100:2:15:1/" title="秋田県秋田市飯島道東2丁目15-2">2</a></li>
	var	self	= this;
	var prefPath    = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/',
		tmpSavePath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/pref/{0}/{1}.csv';
		choTxt     = fs.readFileSync(prefPath + prefCode + '/town-chome-banchi.txt', 'utf-8'),
		choList    = choTxt.split('\n'),//choTxt.match(/[0-9]{5},[0-9]{1,}::[0-9]{1,}/g),
		choSet     = [],
		choRange   = [],
		counter    = 0,
		tmpCmd     = "curl {0}{1}/{2}/ | sed -n -E '/<a.+href.+\\/address\\/[0-9]{5}\\/[0-9]{1,}:[0-9]{0,}:[0-9]{0,}[:0-9]{1,}/p'"
	;
	for(var i = 0; i < choList.length; i++){
		choSet.push(choList[i].split(','));
	}
	console.log(choSet.length);

	if(idxFrom == ''){
		choRange	= choSet;
	}
	else{
		counter	= idxFrom;
		if(idxTo == '')	choRange	= choSet.slice(idxFrom);
		else			choRange	= choSet.slice(idxFrom, idxTo);
	}

	var statement	= '';
	var child		= null;
	return new Promise(function(resolve, reject) {

		choRange.reduce(function(redPromise, current){

			return new Promise(function(resolveLoop, reject){
		//	return redPromise.then(function)
				setTimeout(function () {
					statement	= to.string.format(tmpCmd,
					                                 self.baseURL,
					                                 current[0],
					                                 current[1]);

					cmdExe(statement, function (error, stdout, stderr) {

						console.log(statement);
						if(stdout){
							var fileName = current[0] + '-' + current[1];
							var obj = {
								tgtPath  : prefPath + prefCode + '/town-chome-banchi.txt',
								fileName : fileName,
								savePath : to.string.format(tmpSavePath, prefCode, fileName),
								list     : stdout
							}
							resolveLoop(obj);
						}
						if(error){
							console.error(error);
							reject();
						}
					});
				}, 300);
			})
			.then(function(res){
				return convertToSave(res, counter++);
			});
		}, Promise.resolve([]));
	});

//	getter;
//	return Promise.resolve(counter);
//	return getter;
}
function convertToSave(result, counter){

	return new Promise(function(resolve, reject) {

		var lines	= result.list.split('\n'),
			line	= '',
			lineItem= [],
			item	= '',
			nameCode= [],
			replaced= []
		;

		for(var j = 0; j < lines.length; j++){

			line	= $.trim(lines[j]);
			lineItem= [];

			if(line.match(/<a/)){
				item	= line
						.replace(/^.+<a href="\/address\//, '')
						.replace(/<\/a><\/li>/, '')
						.replace(/".+>/, '');

				nameCode = item.split('/');
				lineItem.push(nameCode[0]);
				lineItem.push(nameCode[1]);
				lineItem.push(nameCode[2]);

				replaced.push(lineItem);
			}
		};
		fs.writeFile(result.savePath, replaced.join('\n') + '\n', (err) => {
			if (err) throw err;

			var state = "sed -i -E '/" + result.fileName.replace('-', ',') + "/d' '" + result.tgtPath + "'";

			cmdExe(state, function (error, stdout, stderr) {

				if(stdout){
					console.log(counter + ' : ' + result.fileName + ' : saved!');
					resolve(true);
				}
				if(error){
					console.error(counter + ' : ' + result.fileName + ' : error');
					reject();
				}
			});
		});
	});
}
crawler.prototype.buildingName = function(){

	var	tempUrl	= 'http://www.homes.co.jp/chintai/hyogo/tokaidosanyohonsen-line/list/?page={0}',
		command	= `curl \'{0}\' |
awk \'match($0, /bukkenName [a-zA-Z\\-]{1,}">.+<\\/span>/) {print substr($0, RSTART, RLENGTH)}\'`,
		tmpSavePath = '/Users/masahiko/MyApp/AppDocuments/MockingData/sampleData/address/addressList/building/{0}.csv';
	;
	var pageList = [],
		startNo  = 1201,
		endNo    = 1400
	;

	for(var i = startNo; i <= endNo; i++){
		pageList.push(i);
	}

	pageList.reduce(function(prev, current, i, ary){

		var promise = new Promise(function(resolve, reject){

			var	homesUrl	= to.string.format(tempUrl, current);
			var statement	= to.string.format(command, homesUrl);

			var child = cmdExe(statement, function (error, stdout, stderr) {

				if(stdout){
					var obj = {
						pageNo   : current,
						savePath : to.string.format(tmpSavePath, current),
						list     : stdout
					}
					resolve(obj);
				}
			});
		})
		.then(function(res){

			var lines	= res.list.split('\n'),
				line	= '',
				item	= '',
				replaced= []
			;

			for(var j = 0; j < lines.length; j++){

				line	= $.trim(lines[j]);
				item	= line
						.replace(/^.+">/, '')
						.replace(/<\/span>/, '');

				if(item.match(/分$/))	continue;
				if(item.match(/km$/))	continue;

				replaced.push(item);
			};

			fs.writeFile(res.savePath, replaced.join('\n'), (err) => {
				if (err) throw err;
				console.log(res.pageNo + ' : saved!');
			});
		});
	});
	return Promise.resolve();
}


// 配列の取得を行うタスク
var taskList = ["a","b","c","d","e"];

// taskAで取得した値を加工するタスク
function taskB(arr) {

	return arr.reduce(function(promise, value) {

		return promise.then(function(editedArray) {

			return taskB_Work(value).then(function(editedElement) {
				editedArray.push(editedElement);
				console.log(editedArray);
				return editedArray;
			});
		});
	}, Promise.resolve([]))
	.catch(function() {
		throw new Error("taskB 処理に失敗");
	});
}


// taskBのreduceで呼び出され、配列要素に対して行う処理
function taskB_Work(value){
	return new Promise(function(resolve, reject) {
		setTimeout(function () {
			console.log("[taskB_Work] value="+value);
			resolve(value + "_edited");
		}, 500);
	});
}

// taskBで加工した配列の値を出力するタスク
function taskC(editedArr) {
	console.log("[taskC]:");
	console.log(editedArr);
};

crawler.prototype.main = function(){
	return taskB(taskList).then(taskC)
			.then(function(){
				console.log("complete!");
			});
}



module.exports = crawler;