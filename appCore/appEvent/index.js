/**
 *
 **/

module.exports = {
	grid : {
		head : function(){
			
			var grid  = $('#confirmGrid'),
				gHead = $('thead', grid),
				gBody = $('tbody', grid),
				colHead,
				iCol  = 0,
				cssGrid = cssClass.grid
			;
			if(arguments.length == 1){
				colHead	= $('th:nth-child(' + arguments[0] + ')', gHead);
			}
			else{
				colHead	= $('th', gHead);
			}
			colHead.each(function(idx, elm){

				$(this).off('click', '**');
				$(this).on('click', function(){
					
					iCol	= $('th', gHead).index($(this));
					
					$("tr th", gHead).removeClass(cssGrid.head.selected);
					$('tr td', gBody).removeClass(cssGrid.body.selected);
					$(this).addClass(cssGrid.head.selected);
					$('tr td:nth-child(' + (iCol + 1) + ')', gBody).addClass(cssGrid.body.selected);
					current.column.index = iCol;
					
					if(current.setPatternName){
						appSrc.pattern.save(project[current.groupName][current.setPatternName]);
					}
				});
				
				$(this).off('contextmenu', '**');
				$(this).on('contextmenu', function(){
					
					iCol	= $('th', gHead).index($(this));
					
					$("tr th", gHead).removeClass(cssGrid.head.selected);
					$(this).addClass(cssGrid.head.selected);
					current.column.index = iCol;
					append.contextMenu.gridHead($(this));
					
					if(current.setPatternName){
						appSrc.pattern.save(project[current.groupName][current.setPatternName]);
					}
				});
			});
		}
	},
	suggest : function(elm){
		
		if(current.column.nameList.length == 0)	return;
		
		elm.click(function(){
			$('+ul', $(this)).css({"display":"block"});		
		});
		elm.change(function(){
			$('+ul', $(this)).css({"display":"none"});		
		});
	},
	project : function(){
		
		var sideBar= $('.sidebar')
		;
		$('.pattern-group > div', sideBar).click(function(e) {
			
			$(this).on('contextmenu', function(){
				
				console.log($(this).text());
				append.contextMenu.projectGroup($(this));
			});
		});
	}
	
}