const 
$  = require('jquery'),
path	= require('path'),
fs		= require('fs'),
JSON5	= require('json5'),
validation	= JSON5.parse(fs.readFileSync(__dirname + '/validate.json5', 'utf-8')),
errLabel  = appSrc.label.subject.error
;
var validParam	= {},
	invalidParam= {},
	errSubject  = '',
	errMessage	= {}
;

module.exports = {
	parameter : {
		valid : {},
		names : {
			using : {},
			require : {}
		}
	},
	error : {
		subject : errMessage,
		message : errMessage
	},
	isValid : function(useParam){
		
		var self = this;
		
		self.parameter.names.using	= useParam;
		
		validParam	= {};
		if(hasEmpty()){
			self.error.subject	= errLabel.form;
			return false;
		}				
		if(!isCorrect(useParam)){
			self.error.subject	= errLabel.form;
			return false;
		}
		
		self.parameter.valid	= validParam;
	
	//	console.log(validParam);
		return true;
	}	
}

function hasEmpty(){
	var has	= false,
		errId	= ''
	;
	$('.required').each(function(){

		if($.trim($(this).val()) == ''){
			errId	= $(this).attr('id');
			errMessage[errId]	= validation[errId].name + validation[errId].message.empty;
			has	= true;
		}
	});
	return has;
}
function isCorrect(paramNames){
	
	var succeed	= true;
	$.each(paramNames, function(i, name){
		
		var	validRole	= validation[name],
			val			= formValue(name),
			validVal	= '',
			matched		= true
		;
		
		if(val == ''){
			validParam[name]	= validVal;
			return true;
		}
		try{
	//		console.log(validRole);
	//		console.log(name);
		}
		catch(e){
	//		console.log(name + ' : ' + val);
			return true;
		}
		switch(validRole.type){
		case 'int':
			if(parseInt(val, 10) == NaN){
				matched	= false;
				break;
			}			
			var num	= parseInt(val, 10);
			if('min' in validRole){
				if(num < validRole.min){
					matched	= false;
					break;
				}
			}
			if('max' in validRole){
				if(validRole.max < num){
					matched	= false;
					break;
				}
			}
			validVal	= num;
			break;
		
		case 'numeric':
			
			if(parseFloat(val) == NaN){
				matched	= false;
				break;
			}			
			var num	= Number(val);
			if('min' in validRole){
				if(num < validRole.min){
					matched	= false;
					break;
				}
			}
			if('max' in validRole){
				if(validRole.max < num){
					matched	= false;
					break;
				}
			}
			var items	= val.split('.');
			
			if(items[items.length - 1].length != validRole.decimal){
				matched	= false;
				break;
			}
			validVal	= num;
			break;
			
		case 'regex':
			
			matched = (val.match(validRole.pattern));
			validVal	= val;
			break;
			
		case 'bit':
			
			if(typeof(val) == 'boolean'){
				validVal	= val;
				break;
			}
			
			matched =  /^(0|1|true|false|yes|no)$/i.test(val);
			
			if(val.match(/^(1|true|yes)$/i))	validVal	= true;
			else								validVal	= false;
			break;
			
		case 'date':

			if(!isDate(val)){
				matched	= false;
				break;
			}
			var date = new Date(val);
			matched = inDataRange(date, validRole);
			
			if(matched)	validVal = val;
			break;
		
		case 'time':
		
			if(!isTime(val)){
				matched	= false;
				break;
			}
			
			if(matched)	validVal = val;
			break;

		case 'text':
			
			var valLen	= val.length;
			if(valLen < validRole.min){
				matched	= false;
				break;
			}
			if(validRole.max < valLen){
				matched	= false;
				break;
			}
			validVal = val;
			break;
			
		case 'byte':
			
			var byteLen = (encodeURIComponent(val).replace(/%../g,"x").length);
			if(byteLen < validRole.min){
				matched	= false;
				break;
			}
			if(validRole.max < byteLen){
				matched	= false;
				break;
			}
			validVal = val;
			break;
		
		case 'list':
		
			var list = validRole.item;

			if(Array.isArray(val)){
				$.each(val, function(idx, elm){
					var match = list.filter(function(item, i){
						if(item == val)	return true;
					});
					if(!match){
						matched	= false;
						return false;
					}
				});
			}
			else{
				var match = list.filter(function(item, i){
					if(item == val)	return true;
				});
				if(!match){
					matched	= false;
					return false;
				}
			}
			if(matched)	validVal = val;
			break;
		
		case 'directory':
			
			matched	= fs.statSync(val).isDirectory();
			
			if(matched)	validVal = val;
			
			break;

		case 'filePath':
			
			matched	= fs.statSync(val).isFile();
			
			if(matched)	validVal = val;
			
			break;
			
		case 'masterKey':

			var tgtObj	= targetObject(validRole.keys);
			
			matched	= (val in tgtObj);
			
			if(matched)	validVal = val;
			
			break;
		
		case 'masterValue':
		
			var tgtObj	= targetObject(validRole.keys);
			var match	= false;
			for(key in tgtObj){
				if(tgtObj[key] == val){
					match	= true;
					break;
				}
			}
			matched	= match;
			if(matched)	validVal = val;
			break;
		
		case "check":

			validVal = val;
			break;
			
		default:	
			validVal = val;
			break;
		}
		
		if(matched){
			validParam[name]	= validVal;
		}
		else{
			invalidParam[name]	= val;
			errMessage[name]	= validRole.name + validRole.message.invalid;
			succeed	= false;
		}		
	})
	return succeed;
}

function formValue(name){

	var elmCheck = $(':checkbox[name=' + name + ']:checked');
	
	if(0 < elmCheck.length){
		var valList	= [];
		elmCheck.each(function(idx, elm){
			valList.push($(this).val());
		});
		return valList;
	}
	
	var elmRadio = $(':radio[name=' + name + ']:checked');
	if(0 < elmRadio.length){
		return elmRadio.val();
	}
	
	var elm     = $('#' + name),
		tagName	= elm[0].tagName.toLowerCase(),
		frmType = ''
	;
	
	switch(tagName) {
	case 'input':
		frmType	= elm[0].type;
		break;
	case 'select':
	case 'textarea':
		frmType	= tagName
		break;
	}
	
	if(frmType == 'checkbox')	return elm.is(':checked');
	if(frmType == 'radio')		return elm.is(':checked');	
	if(frmType == 'select')		return $('option:selected', elm).val();
	return $.trim(elm.val());
}

function targetObject(keys){
	
	var	obj	= appSrc.master;
	
	$.each(keys, function (i, key){
		obj	= obj[key];
	})
	return obj;
}
function isDate(s){
	var matches = /^(\d+)\-(\d+)\-(\d+)$/.exec(s);
	if(!matches)	return false;

	var y = parseInt(matches[1]),
		m = parseInt(matches[2]),
		d = parseInt(matches[3])
	;
	if(m  < 1)	return false;
	if(12 < m)	return false;
	if(d  < 1)	return false;
	if(31 < d)	return false;

	//日付オブジェクトを生成する->閏年以外の2/29は3/1になるため、
	//下の条件式で[false]になり、正しく日付を精査できる
	var dt = new Date(y, m - 1, d, 0, 0, 0, 0);
	if(dt.getFullYear() != y)		return false;
	if(dt.getMonth()    != m - 1)	return false;
	if(dt.getDate()     != d)		return false;
	return true;
}
function isTime(s){
	var matches = /^([0-9]{1,2})\:([0-9]{1,2})$|^([0-9]{1,2})\:([0-9]{1,2})\:([0-9]{1,2})/.exec(s);
	if(!matches)	return false;

	var h = 0,
		m = 0,
		s = 0
	;
	if(matches[1] !== undefined){
		h	= parseInt(matches[1]);
		m	= parseInt(matches[2]);
	}
	if(matches[3] !== undefined){
		h	= parseInt(matches[3]);
		m	= parseInt(matches[4]);
		s	= parseInt(matches[5]);
	}
	if(h < 0)	return false;
	if(m < 0)	return false;
	if(s < 0)	return false;
	if(23 < h)	return false;
	if(59 < m)	return false;
	if(59 < s)	return false;
	return true;
}
function inDataRange(d, obj){

	if('min' in obj){
		var min	= new Date(obj.min);
		if(d.getTime() < min.getTime()){
			return false;
		}
	}
	if('max' in obj){
		var max	= new Date(obj.min);
		if(max.getTime() < d.getTime()){
			obj
		}
	}
	return true;
}