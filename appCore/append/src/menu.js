const 
FS	= require('fs'),
FSE = require('fs-extra')
;

module.exports = {
	gridHead : function(obj){
		timeInterval(column(obj));
	},
	project : {
		group : function(grpName){
			timeInterval(group(grpName));
		},
		setPattern : function(grpName, ptnName){
			timeInterval(setPattern(grpName, ptnName));
		}
	}
}

function timeInterval(menuObj){
	var m = Menu.buildFromTemplate(menuObj);
	var i = 0;
	var	t = setInterval(function() {
			i++;
			if (i == 3) {
				clearInterval(t);
				m.popup(remote.getCurrentWindow());
			}
		}, 20);
}

function column(obj){
	return [
		{
			label: 'Data Typeを編集',
			click: function(){
				var iCol	= current.column.index,
					ptnName = current.patternList[iCol].type.toLowerCase(),
					paramLen= Object.keys(current.patternList[iCol].param)
				;
				if(paramLen == 0){
					var msg = {
						subject : 'Data Typeが設定されていません。',
						message : 'ツールバーメニュー「Data Type」から設定してください。'
					}
					appSrc.alert.info(msg);
					return;
				}
				append.modal.open(ptnName);
			}
		},
		{
			type : 'separator'
		},
		{
			label: '列を前に追加',
			click: function(){
				console.log(current);
				
			}
		},
		{
			label: '列を後ろに追加',
			click: function(){
				console.log(current);
			}
		},
		{
			type : 'separator'
		},
		{
			label: '列名の変更',
			click: function(){
				
				var colNo	= current.column.index + 1,
					gHead	= $('#confirmGrid thead tr'),
					th		= $('th:nth-child(' + colNo + ')', gHead),
					colName	= th.text();
					inputTag= '<input id="colName" type="text" class="edit" style="width:{0}px;height{1}px:" value="{2}">',
					w		= th.width() + 30,
					h		= th.height() + 4
				;
				th.css({padding:0,width:w,height:h}).text('');
				th.append(to.string.format(inputTag, w, h, colName));
				
				var elmColName = $('#colName', gHead);
				
				elmColName.select();
				elmColName.blur(function(){
					var newName	= $(this).val(),
						iCol	= current.column.index
					;
					current.column.nameList[iCol] = newName;
					current.patternList[iCol].name = newName;
					
					if(current.setPattern){
						appSrc.pattern.save(project[current.group][current.setPattern]);
					}
					
					elmColName.remove();
					th.text(newName).css({padding:'2px 15px'});
				});
			}
		},
		{
			label: '削除',
			click: function(){
				current.column.nameList.splice(current.column.index, 1);
				current.patternList.splice(current.column.index, 1);
				
				var gHead = $('#confirmGrid thead'),
					gBody = $('#confirmGrid tbody'),
					colNo = current.column.index + 1
				;
				$('th:nth-child(' + colNo + ')', gHead).remove();
				$('tr td:nth-child(' + colNo + ')', gBody).remove();
			}
		}
	];
}
function group(grpName){
	
	current.group = grpName;
	return [
		{
			label: 'グループ名を変更',
			click: function(){
				patternModal('editGroup');
			}
		},
		{
			label: 'グループを削除',
			click: function(){
				
				var grpNode = project[grpName],
					prjPath = app.getPath('userData') + '/CurrentProject/',
					prnPath = ''
				;
				$.each(grpNode, function(idx, elm){
	
					prnPath	= prjPath + elm;
					FSE.remove(prnPath, function (e) {
						if(e){
							console.error(e);
						}
					});
				});
				
				delete project[grpName];
				appSrc.project.save();
				defaultView();
			}
		},
		{
			type : 'separator'
		},
		{
			label: 'セットパターンを追加',
			click: function(){
				patternModal('editSetPattern');
			}
		}
	];
}
function setPattern(grpName, ptnName){
	
	current.group		= grpName;
	current.setPattern	= ptnName;
	return [
		{
			label: 'セットパターン名を変更',
			click: function(){
				patternModal('editSetPattern');
			}
		},
		{
			label: 'セットパターンを削除',
			click: function(){
				
				var ptnFile = project[grpName][ptnName],
					prnPath = app.getPath('userData') + '/CurrentProject/' +  ptnFile
				;
			
				FSE.remove(prnPath, function (e) {
					if(e){
						console.error(e);
					}
				});
				
				delete project[grpName][ptnName];
				appSrc.project.save();
				defaultView();
			}
		}
	];
}