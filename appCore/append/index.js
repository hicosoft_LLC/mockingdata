/**
 *	@module	append 
 */
const
appSrc  = require('../sources'),
dataBind	= require('../dataBind')
;

module.exports = {
	/**
	 *
	 */
	modal : {
		open : function(srcName){
			var elmModal = $('.modal-contents');
			elmModal.children().remove();
			if(!(srcName in appSrc.html)){
				console.log(srcName);
				return;
			}
			elmModal.append(appSrc.html[srcName]);
			$('.modal').show();
		},		
		close : function(){
			$(".modal").hide();
			$(".modal-contents").children().remove();
		}
	},
	help : {
		open : function(btnId, src, w = 600){
			
			$('#' + btnId).click(function(){
				window.open(src, "", "width=" + w + ",height=500");
			});
		}	
	},
	grid : {
		clear : function(){
			var	grid  = $('#confirmGrid'),
				tHead = $('thead tr', grid),
				tBody = $('tbody', grid)
			;
			tHead.children().remove();
			tBody.children().remove();
		},
		head : function(columnList){
			var self = this,
				grid  = $('#confirmGrid'),
				tHead = $('thead tr', grid),
				tmpTh = '<th>{0}</th>',
				list  = []
			;
			$.each(columnList, function(i, elm){
				list.push(to.string.format(tmpTh, elm));
			});
			tHead.children().remove();
			tHead.append(list.join('\n'));
			appEvent.grid.head();
		},
		row : function(){
			
			var gBody = $('#confirmGrid tbody'),
				rowLen   = $('tr', gBody).size()
			;
			if(current.maxRows <= rowLen)	return;
			
			for(i = 0; i < current.maxRows; i++){			
				gBody.append('<tr></tr>');
			}
		},
		emptyCell : function(newColumn){
			
			var	gBody = $('#confirmGrid tbody');
			
			$.each(newColumn, function(i, name){
				$('tr', gBody).each(function(j, row){
					$(this).append('<td>&nbsp;</td>');
				});
			});
		},
		column : {
			add : function(){
				var gHead      = $('#confirmGrid thead'),
					iNew       = current.column.nameList.length,
					colNo      = iNew + 1,
					defValue   = appSrc.config.default,
					newColName = defValue.columnName,
					oPattern   = defValue.setPattern
				;
				oPattern.name	= newColName;
				current.column.maxNo++;
				current.column.index	= iNew;
				current.column.nameList.push(newColName);
				current.patternList.push(oPattern);
				console.log(current);
							
				$('tr', gHead).append('<th>' + newColName + current.column.maxNo + '</th>');
				appEvent.grid.head(colNo);
			
				return newColName;
			},
			remove : function(){
				
				var grid = $('#confirmGrid'),
					hRow = $('thead tr', grid),
					bRow = $('tbody tr',grid),
					cntSelected = $('.select-head', hRow).size(),
					iSelected = $("th", hRow).index($('.select-head', hRow)),
					numSelected = iSelected + 1;
									
				if(cntSelected === 0){
					appSrc.alert.info(appSrc.message.column.warning);
					return;
				}
				current.column.nameList.splice(iSelected, 1);
				current.column.optionTag.splice(iSelected, 1);
				current.patternList.splice(iSelected, 1);

				$('th:nth-child(' + numSelected + ')', hRow).remove();
				$('td:nth-child(' + numSelected + ')', bRow).remove();
			}			
		},
		data : function(){
			
			var gBody	= $('#confirmGrid tbody'),
				list	= [],
				iMax    = 0,
				iTr		= 0,
				iTd		= current.column.index + 1
			;
			
			if(0 < arguments.length)	list = arguments[0];
			if(1 < arguments.length)	iTd  = arguments[1] + 1;
			
			iMax = list.length -1;
			$.each(list, function(i, elm){
				
				if(iMax < i)	return false;
				
				iTr	= i + 1;
				$('tr:nth-child(' + iTr + ') td:nth-child(' + iTd + ')').text(elm);				
			});
		},
		dataAll : function(dataList){
			
			var grid  = $('#confirmGrid'),
				gridRow = $('tbody tr', grid)
			;
			$.each(dataList, function(i, list){				
				$.each(gridRow, function(j, elm){
					$('td:nth-child(' + (i + 1) + ')', $(this)).text(dataList[i][j]);
				});				
			});
		},
		recycle : function(){
			var grid = $('#confirmGrid');
			$('thead tr', grid).children().remove();
			$('tbody', grid).children().remove();
		},
		refresh : function(){
			console.log(current);
		}
	},
	contextMenu : require('./src/menu.js'),
	form : {
		columnOption : function(){
						
			var tags = dataBind.selectOptions(current.column.nameList, appSrc.config.blank.add),
				iCol = current.column.index + 2,
				elmCol	= elmCol	= $('#column'),
				selectedIdx = iCol
			;
			if(1 <= arguments.length)	elmCol	= $('#' + arguments[0]);			
			if(arguments.length == 2)	selectedIdx	= arguments[1];
			
			elmCol.children().remove();
			elmCol.append(tags.join('\n'));
			$('option:nth-child(' + selectedIdx + ')', elmCol).attr('selected', true);
			
			elmCol.change(function(){
				if($(this).val() == '')	return;
				current.column.index = parseInt($(this).val());
				$('#confirmGrid thead th:nth-child(' + (current.column.index + 1) + ')').click();
			})
		},
		comboBox : function(opts){
			
			var elmTxt	= $('#' + opts.formId),
				elmSug	= elmTxt.next()
			;
			elmSug.append(opts.suggestTag);
			
			$('li', elmSug).each(function(){
				$(this).click(function(){
					elmTxt.val($(this).text());
					elmSug.css({'display':'none'})
				});
			});
			$('body').off('click');
			$('body').on('click', function(evt){
				var elm = evt.toElement,
					id  = elm.id,
					css = elm.className
				;
				if(id != opts.formId && css !== 'suggest'){
					elmSug.css({"display":"none"});
				}
			});
		},
		columnComboBox : function(){
			
			if(current.column.nameList.length == 0)	return;
			
			var tags    = (1 < arguments.length)? arguments[1] : dataBind.comboBox(current.column.nameList),
				arg     = (0 < arguments.length)? arguments[0] : 'column',
				argType = comboArgType(arguments[0]),
				keys    = comboKeys(arguments),
				iCol	= current.column.index,
				currentColName = '',
				elmCol, 
				elmSug			
			;
			
			if(tags.length == 0)	return;	
			if(isFinite(iCol))		currentColName	= current.column.nameList[iCol];
			
			$.each(keys, function(idx, elm){
				
				elmCol	= $('#' + elm);
				elmSug  = elmCol.next();
				
				elmCol.val(columnName(arg, arg[elm], currentColName));
				elmSug.append(tags);
				$('li', elmSug).each(function(){
					$(this).click(function(){
						elmCol.val($(this).text());
						elmSug.css({'display':'none'})
					})
				});
			});
			$('body').off('click');
			$('body').on('click', function(evt){
				var elm = evt.toElement,
					id  = elm.id,
					css = elm.className
				;
				if(keys.indexOf(id) < 0 && css !== 'suggest'){
					elmSug.css({"display":"none"});
				}
			});
		}
	}
}

function comboArgType(arg){
	
	if(Array.isArray(arg))		return 'array';
	if(typeof(arg) == 'object')	return 'object';
	return 'string';
}
function comboKeys(arg){
	
	if(arg.length < 0)	return ['column'];
	
	if(Array.isArray(arg[0]))		return arg[0];
	if(typeof(arg[0]) == 'object')	return Object.keys(arg[0]);
	
	return [arg[0]];
}

function columnName(arg, getType, currentName){
	
	if(Array.isArray(arg))		return currentName;
	if(typeof(arg) != 'object')	return currentName;	
	if(getType == 'current')	return currentName;
	
	var nextIdx	= current.column.index + 1;
	
	if(current.column.nameList.length < nextIdx + 1)	return '';
	
	return current.column.nameList[nextIdx];

}
