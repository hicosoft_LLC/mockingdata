SELECT
	*,
	RANDOM() as rand
FROM (
	SELECT
		kanji.firstNameKanji,
		kana.firstNameKana,
		kanji.gender
	FROM		firstNamekana	kana
	INNER JOIN	firstNamekanji	kanji
			ON	kana.firstNameKanaID = kanji.firstNameKanaID
	WHERE	gender = 0
	ORDER BY	RANDOM()
	limit	{0}
)

UNION ALL 

SELECT
	*,
	RANDOM() as rand
FROM (
	SELECT
		kanji.firstNameKanji,
		kana.firstNameKana,
		kanji.gender
	FROM      	firstNamekana	kana
	INNER JOIN	firstNamekanji	kanji
			ON	kana.firstNameKanaID = kanji.firstNameKanaID
	WHERE	gender = 1
	ORDER BY	RANDOM()
	limit	{1}
)
ORDER BY rand