UPDATE addr_building SET
	viewNum = RANDOM() % 10,
	orderNum = RANDOM()
;
SELECT
	CASE
	WHEN viewNum < 2 THEN ''
	ELSE buildingName
	END AS buildingName,
	
	CASE
	WHEN viewNum < 2 THEN ''
	ELSE buildingKanaH
	END AS buildingKanaH,
	
	CASE
	WHEN viewNum < 2 THEN ''
	ELSE buildingKanaK
	END AS buildingKanaK,
	
	CASE
	WHEN viewNum < 2 THEN ''
	ELSE buildingKanaHK
	END AS buildingKanaHK
FROM	addr_building
ORDER BY orderNum ASC
LIMIT	{0}