/*
	1. まず、必要件数を取得する
	2. 取得したデータをJSの配列に格納する
	3. viewNumを更新する
*/
SELECT
	addr.*,
	cho.chomeName,
	cho.chomeKanaH,
	cho.chomeKanaK,
	cho.chomeKanaHK
FROM		address		AS	addr
LEFT  JOIN	addr_chome	AS	cho
		ON	cho.cityCode	= addr.cityCode
	   AND	cho.townCode	= addrtownCode
ORDER BY viewNum ASC
LIMIT {0}
;

UPDATE address SET
	viewNum = RANDOM()
;

/*
# 丁目, 番地, 支号(枝番), 
✔︎	cho				01202,125:1,1丁目	cityCode,	townCode :chomeCode,						chomeName

✔︎	ban				01202,1::10,10		cityCode,	townCode :0			:banCode,				banName
✔︎	city-ban		,,01430,::10,999,	,,cityCode,	0		 :0			:banCode,				banName
✔︎	town-ban		03201,101::3,30,	cityCode,	townCode :0		 	:banCode,				banName
✔︎	tonw-chome-ban		01202,101:2:10,10	cityCode,	townCode :chomeCode	:banCode,			banName

✔︎	town-chome-ban-gou	03201,1:2:10:8,20	cityCode,	townCode :chomeCode	:banCode  :goCode,	goName
✔︎	town-ban-gou		01202,1::12:5,22	cityCode,	townCode :0			:banCode  :goCode,	goName
*/