SELECT
	*
FROM	(
	SELECT
		kanji.lastNameKanji,
		kana.lastNameKana
	FROM	(
		SELECT
			*
		FROM	lastNameKanji kanji
		INNER JOIN (
			SELECT
				lastNameKanjiID,
				lastNameKana
			FROM	lastNameKana
						
		) AS kana
				ON	kana.lastNameKanjiID = kanji.lastNameKanjiID
		WHERE	weight > 400000
	)
)	
UNION ALL

SELECT
	*
FROM	(
	SELECT
		kanji.lastNameKanji,
		kana.lastNameKana
	FROM	lastNameKanji kanji
	INNER JOIN (
		SELECT
			lastNameKanjiID,
			lastNameKana
		FROM	lastNameKana
		
		ORDER BY RANDOM()
	) AS kana
			ON	kana.lastNameKanjiID = kanji.lastNameKanjiID
	WHERE	weight > 300000
)
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 200000
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 100000
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 90000
	--	abs(random()) % 100;
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 80000
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 70000
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 60000
	
	UNION ALL

	SELECT 
		lastNameKanjiID,
		lastNameKanji
	FROM lastNameKanji
	WHERE weight > 50000
)
ORDER BY RANDOM()
LIMIT 5000