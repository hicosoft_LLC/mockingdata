SELECT
	organizationType,
	domainTopName,
	isTop
FROM (
	SELECT
		organizationType
	FROM	domainOrg
	WHERE weight > ABS(RANDOM()) % 3440	
),
(
	SELECT
		domainTopName,
		weight,
		isTop
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000

	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop	
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
	
	UNION ALL 

	SELECT
		domainTopName,
		weight,
		isTop		
	FROM	domainTop
	WHERE	weight >= ABS(RANDOM()) % 120000000
)
ORDER BY RANDOM()