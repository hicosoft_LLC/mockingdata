SELECT
	*
FROM (
	SELECT
		kanji.lastNameKanji,
		kana.lastNameKana
	FROM		lastNameKanji	AS kanji
	INNER JOIN	lastNameKana	as kana
			ON	kana.lastNameKanjiID	= kanji.lastNameKanjiID
		   AND	kana.weight > 0
	WHERE	kanji.weight > 201046
	ORDER BY RANDOM()
	LIMIT	100
), (
	SELECT
		kanji.firstNameKanji,
		kana.firstNameKana,
		kanji.gender
	FROM		firstNameKanji	AS kanji
	INNER JOIN	firstNameKana	as kana
			ON	kana.firstNameKanaID	= kanji.firstNameKanaID
	ORDER BY RANDOM()
	LIMIT	100
)
ORDER BY RANDOM()
LIMIT 100