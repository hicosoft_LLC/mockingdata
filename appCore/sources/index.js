/**
 *
 **/
const 
electron = require('electron'),
app      = electron.app || electron.remote.app,
dialog   = electron.dialog || electron.remote.dialog,
FS       = require('fs'),
FSE      = require('fs-extra'),
PATH     = require('path'),
RL       = require('readline'),
JSON5    = require('json5'),
JSCHARDET= require('jschardet'),
ICONV    = require('iconv').Iconv,
$		 = require('jquery'),
fstream  = require('fstream'),
zlib	 = require("zlib"),
tar		 = require("tar")
;
var	osLang	= app.getLocale(),
	lang	= (osLang == 'ja')?	'ja' : 'en',
	usrDataPath = app.getPath('userData'),
	currentPath = usrDataPath + '/CurrentProject',
	tempPath    = usrDataPath + '/MockingProject',
	projectPath = currentPath + '/project.json5',
	longTextFileName = 'long-text.txt'
;	

module.exports = {
	locale  : lang,
	html    : getSource('html'),
	tag     : getSource('tag'),
	help    : getPath('help'),
	sql     : getSource('sql'),
	label   : getLangData('label.json5'),
	hint    : getLangData('hint.json5'),
	message : getLangData('message.json5'),
	master  : getLangData('master.json5'),
	longtext: function(filePath){	return getLongText(filePath);	},
	config  : getConfig(),
	project : {
		get  : function(){	return getProject();	},
		save : function(){	return saveProject();	}
	},
	pattern : {
		get  : function(fileName){	return	getPatterns(fileName);	},
		save : function(fileName){			savePatterns(fileName);	}
	},
	appFile : {
		deploy : function(){	return deployAppFile();	},
		save   : function(){	return saveAppFile();	}
	},
	userFile   : function(){	
				
		var extnList = ['txt','csv','json','json5','xml', '*'];
		
		if(arguments.length == 1)	extnList	= arguments[0];
		   
		return userCsv(extnList)[0];	
	},
	exportFile : function(){	return exportDialog();	},
	saveFile   : function(){	return saveDialog();	},
	alert : {
		info : function(obj){
			obj.buttons = ['OK'];
			showMessageBox(obj);
		},
		confirm : function(obj){
			obj.buttons = ['Yes','No'];
			return showMessageBox(obj);
		},
		error : function(sbj, obj){
			
			var msg  = [], info = {};
						
			$.each(obj, function(idx, elm){
				msg.push(elm);
			});
			
			info.buttons = ['OK'];
			info.subject = sbj;
        	info.message = msg.join('\n');
        	
			showMessageBox(info);
		}		
	},
	parseCsv : function(filePath){
		
		var fileBody = fs.readFileSync(filePath, 'utf8'),
			lineList = '',
			line     = '',
			list     = []
		;
		try{
			lineList = fileBody.split('\n');
		}
		catch(e){
			console.log(e);
			return 
		}
		for(var i = 0; i < lineList.length; i++){
			
			line = $.trim(lineList[i]).replace(/,$/, '').replace(/\t/g, ',').replace(/\"/g, '');
			if(line != ''){
				Array.prototype.push.apply(list, line.split(','));
			}
		}
		return list;
	},
	setPatternFileName : function(param){
		
		//既存のsetPattern
		var grp	= param.groupName,
			ptn = param.setPatternName
		;
		if(grp in project){
			if(ptn in project[grp])	return project[grp][ptn];
		}
		//新しいsetPattern
		var	now	 = to.now('yyyymmddhhnnssf'),
			hash = crypto.createHash('sha1')
						.update(now)
						.digest('hex'),
			enc  = hash + '.json5'
		;
		return enc;
	},
	readLine : function(filePath){
		
		if(!FS.existsSync(filePath)){
			var self = this,
				subject = 'CSVファイルを読み込めませんでした。',
				message = 'ご指定のCSVファイルがあるか今一度お確かめください。'
			;
			self.alert.error(subject, [message])
			return;
		}
		var fileBody = FS.readFileSync(filePath, 'utf-8'),
			lineItem = fileBody.split('\n');
			list     = [],
			line     = ''
		;
		$.each(lineItem, function(i, elm){
			line = $.trim(elm);
			if(line != '')	list.push(line);
		});
		return list;
	}
}
/**
	取得系
*/
function getSource(dirName){
	
	var srcRoot	= __dirname + '/' + dirName + '/',
		extension	= '',
		abPath		= '',
		filename	= '',
		fileObj		= {},
		stat        = null
	;
	var getSrcList = function(srcPath){
		
		var files = FS.readdirSync(srcPath);
		
		for(var i = 0; i < files.length; i++){
			
			abPath		= srcPath + files[i];
			stat = FS.statSync(abPath);
			
			if(stat.isDirectory()){
				getSrcList(abPath + '/');
			}
			else{			
				extension	= PATH.extname(abPath);
				filename	= PATH.basename(abPath, extension);
				fileObj[filename]	= FS.readFileSync(abPath, 'utf-8');
			}
		}
	};	
	getSrcList(srcRoot);
	return fileObj;
}
function getPath(dirName){
	
	var srcRoot	= __dirname + '/' + dirName + '/',
		abPath	= '',
		fileObj	= {}
	;
	var getSrcList = function(srcPath){
		
		var files = FS.readdirSync(srcPath);
		
		for(var i = 0; i < files.length; i++){
			
			abPath		= srcPath + files[i];
			stat = FS.statSync(abPath);
			
			if(stat.isDirectory()){
				getSrcList(abPath + '/');
			}
			else{			
				extension	= PATH.extname(abPath);
				filename	= PATH.basename(abPath, extension);
				fileObj[filename]	= abPath;
			}
		}
	};	
	getSrcList(srcRoot);
	return fileObj;
}
function getLangData(fileName){

	var filePath = __dirname + '/' + lang + '/' + fileName,
		extName = PATH.extname(fileName)		
	;
	
	if(extName == '.txt')	return FS.readFileSync(filePath, 'utf-8');
	
	return getJson(filePath);
}
function getConfig(){
	return getJson(__dirname + '/config.json5');
}
function getProject(){	
	return getJson(projectPath);
}
function getPatterns(fileName){
	return getJson(currentPath + '/' + fileName);
}
function getJson(filePath){
	
	var fileBody = {};
	try{
		fileBody = JSON5.parse(FS.readFileSync(filePath, 'utf-8'));
	}
	catch(e){
		console.log(e);
	}
	return fileBody;
}
function getLongText(filePath){
	
	if(!filePath)					return getLangData(longTextFileName);	
	if(filePath == '')				return getLangData(longTextFileName);			
	if(!FS.existsSync(filePath))	return getLangData(longTextFileName);	
	
	var fileBody   = FS.readFileSync(filePath),
		chardet    = JSCHARDET.detect(fileBody),
		iconveter  = new ICONV(chardet.encoding, 'UTF-8'),
		encBody	   = iconveter.convert(fileBody)
	;
	return	encBody.toString();
}

/**
	登録系
*/
function savePatterns(fileName){
	
	var	patternPath = currentPath + '/' + fileName;
	FS.writeFileSync(patternPath, JSON5.stringify(current, null, '\t'), 'utf-8');
}
function saveProject(){
	
	try{
		FS.writeFileSync(projectPath, JSON5.stringify(project, null, '\t'), 'utf-8');
		return true;
	}
	catch(e){
		console.log(e);
		return true;
	}
}

/**
	Dialog (Open, Save)系
*/
function deployAppFile(){
	var fileList =  dialog.showOpenDialog(
						BrowserWindow.getFocusedWindow(),	
						{
							properties: ['openFile'],
							filters: [
								{
									name: 'appファイル',
									extensions: ['mocking-project']
								}
							]
						}
					)
	;
	if(!fileList)	return'';
	
	var readPath = fileList[0],	
		extn     = PATH.extname(readPath),
		opts     =	{
						clobber : true
					}
	;
	if(extn  != '.mocking-project'){		
		var dialogObj = {
				buttons : ['OK'],
				subject : '読み込みエラー',
				message : 'MockingDataのデータファイルではありません。'
			}
		;
		showMessageBox(dialogObj);
		return;
	}
	
	FSE.emptyDir(tempPath, function(et){
		if(!et){
			var reader = fstream.Reader({ 'path': readPath})
						.pipe(zlib.Gunzip())
						.pipe(tar.Extract())
						.pipe(fstream.Writer({ 'path': tempPath, 'type': 'Directory'}));
			
			reader.on('end', function(){
				FSE.emptyDir(currentPath, function(ec){
					if(ec)	console.log(ec);
					
					FSE.copySync(tempPath + '/MockingProject/', currentPath + '/', opts);					
				});
			});			
		}
		else{
			console.log(et);
		}
	});
}
function saveAppFile(){
	var savePath = saveDialog(),
		opts     =	{
						clobber : true
					}
	;

	FSE.emptyDir(tempPath, function(e){
		if(!e){
			FSE.copySync(currentPath + '/', tempPath + '/', opts);
			
			try{
				fstream.Reader({ 'path': tempPath, 'type': 'Directory' })	
				.pipe(tar.Pack())	
				.pipe(zlib.createGzip())
				.pipe(fstream.Writer({ 'path': savePath }));

			}
			catch(e){
				console.log(e);
			}
		}
		else{
			console.log(e);
		}
	});
}
function userCsv(extn){
	return dialog.showOpenDialog(BrowserWindow.getFocusedWindow(),	
		{
			properties: ['openFile'],
			filters: [
				{
					name: 'appファイル',
					extensions: extn
				}
			]
		}
	);
}
function exportDialog(){
	var opts = {
	    	title : 'Export Path'
		}
	;
	var path = dialog.showSaveDialog(opts);
	if(path){
		fs.writeFileSync(path, '', 'utf-8');
		return path;
	}
	else{
		return '';
	}
}
function saveDialog(){
	var opts = {
			title: 'Save project',
			filters: [
				{ name: 'MockingData File', extensions: ['mocking-project', 'zip']}
			]
		};

	return dialog.showSaveDialog(opts);
}
/*
	Dialog (Info) 系
	
	[options]
	type	メッセージボックスのタイプを設定する。(info, 'warning'のいずれかを文字列で指定)
	buttons	メッセージボックスのボタンを設定。文字列の配列として設定します。
	title	タイトルを設定する
	message	メッセージを設定
	detail	メッセージボックスの詳細メッセージを設定
	icon	ダイアログに表示するアイコン画像を設定。
*/
function showMessageBox(obj) {
    var opts = {
        type: 'info',
        buttons: obj.buttons,
        message: obj.subject,
        detail: obj.message
    };
    
    //戻り値[e]は[opts.buttons]の要素Index。画面上は逆向きに並べられる
    var btnIdx = dialog.showMessageBox(BrowserWindow.getFocusedWindow(), opts);
    return btnIdx;
}
function showErrorBox() {
	dialog.showErrorBox("タイトル", "本文");
}



